<?php

function delete_directory($dirname) {
         if (is_dir($dirname))
           $dir_handle = opendir($dirname);
	 if (!$dir_handle)
	      return false;
	 while($file = readdir($dir_handle)) {
	       if ($file != "." && $file != "..") {
	            if (!is_dir($dirname."/".$file))
	                 unlink($dirname."/".$file);
	            else
	                 delete_directory($dirname.'/'.$file);
	       }
	 }
	 closedir($dir_handle);
	 rmdir($dirname);
	 return true;
}

echo "CACHE FOLDERS (cache found and deleted):\n";

    $contents = glob('cache/*');
    $content_count = 0;
    foreach($contents as $content){ 
        if( is_dir($content) ) {
            delete_directory($content);
            $content_count++;
            $total++;
        }
    }
echo "TOTAL CACHE FOUND and DELETED: " . $content_count;
