-- Table structure for table `facebook_accounts` 

CREATE TABLE `facebook_accounts` (
  `user_id` int(20) DEFAULT NULL,
  `facebook_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`facebook_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_id` (`user_id`)
);

-- Table structure for table `google_accounts` 

CREATE TABLE `google_accounts` (
  `user_id` int(20) DEFAULT NULL,
  `google_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`google_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_id` (`user_id`)
);

-- Table structure for table `recovery_codes` 

CREATE TABLE `recovery_codes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `code` varchar(200) NOT NULL,
  `expiry` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `users` 

CREATE TABLE `users` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `passwd` text NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'user',
  `verified` int(1) NOT NULL DEFAULT '0',
  `code` text,
  `date_registered` datetime DEFAULT CURRENT_TIMESTAMP,
  `referred_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
);

