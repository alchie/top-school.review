-- Table structure for table `users` 

CREATE TABLE `users` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `passwd` text NOT NULL,
  `type` varchar(20) NOT NULL DEFAULT 'user',
  `verified` int(1) NOT NULL DEFAULT '0',
  `code` text,
  `date_registered` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
);

