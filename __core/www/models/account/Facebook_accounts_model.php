<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Facebook_accounts_model Class
 *
 * Manipulates `facebook_accounts` table on database

CREATE TABLE `facebook_accounts` (
  `user_id` int(20) DEFAULT NULL,
  `facebook_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`facebook_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_id` (`user_id`)
);

ALTER TABLE  `facebook_accounts` ADD  `user_id` int(20) NULL   UNIQUE KEY;
ALTER TABLE  `facebook_accounts` ADD  `facebook_id` varchar(100) NOT NULL   PRIMARY KEY;
ALTER TABLE  `facebook_accounts` ADD  `name` varchar(100) NOT NULL   ;
ALTER TABLE  `facebook_accounts` ADD  `email` varchar(100) NOT NULL   UNIQUE KEY;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Facebook_accounts_model extends MY_Model {

	protected $user_id;
	protected $facebook_id;
	protected $name;
	protected $email;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'facebook_accounts';
		$this->_short_name = 'facebook_accounts';
		$this->_fields = array("user_id","facebook_id","name","email");
		$this->_required = array("name","email");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: user_id -------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('user_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `user_id` variable
	* @access public
	*/

	public function getUserId() {
		return $this->user_id;
	}
	
// ------------------------------ End Field: user_id --------------------------------------


// ---------------------------- Start Field: facebook_id -------------------------------------- 

	/** 
	* Sets a value to `facebook_id` variable
	* @access public
	*/

	public function setFacebookId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('facebook_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `facebook_id` variable
	* @access public
	*/

	public function getFacebookId() {
		return $this->facebook_id;
	}
	
// ------------------------------ End Field: facebook_id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

	public function getEmail() {
		return $this->email;
	}
	
// ------------------------------ End Field: email --------------------------------------



	
	public function get_table_options() {
		return array(
			'user_id' => (object) array(
										'Field'=>'user_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'UNI',
										'Default'=>'',
										'Extra'=>''
									),

			'facebook_id' => (object) array(
										'Field'=>'facebook_id',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>''
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'email' => (object) array(
										'Field'=>'email',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'UNI',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'user_id' => "ALTER TABLE  `facebook_accounts` ADD  `user_id` int(20) NULL   UNIQUE KEY;",
			'facebook_id' => "ALTER TABLE  `facebook_accounts` ADD  `facebook_id` varchar(100) NOT NULL   PRIMARY KEY;",
			'name' => "ALTER TABLE  `facebook_accounts` ADD  `name` varchar(100) NOT NULL   ;",
			'email' => "ALTER TABLE  `facebook_accounts` ADD  `email` varchar(100) NOT NULL   UNIQUE KEY;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Facebook_accounts_model.php */
/* Location: ./application/models/Facebook_accounts_model.php */
