<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_reviews_model Class
 *
 * Manipulates `places_reviews` table on database

CREATE TABLE `places_reviews` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(100) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `author_id` varchar(100) NOT NULL,
  `profile_photo_url` text NOT NULL,
  `rating` int(10) NOT NULL,
  `text` text NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `places_reviews` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `places_reviews` ADD  `place_id` varchar(100) NOT NULL   ;
ALTER TABLE  `places_reviews` ADD  `author_name` varchar(100) NOT NULL   ;
ALTER TABLE  `places_reviews` ADD  `author_id` varchar(100) NOT NULL   ;
ALTER TABLE  `places_reviews` ADD  `profile_photo_url` text NOT NULL   ;
ALTER TABLE  `places_reviews` ADD  `rating` int(10) NOT NULL   ;
ALTER TABLE  `places_reviews` ADD  `text` text NOT NULL   ;
ALTER TABLE  `places_reviews` ADD  `time` int(10) NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_reviews_model extends MY_Model {

	protected $id;
	protected $place_id;
	protected $author_name;
	protected $author_id;
	protected $profile_photo_url;
	protected $rating;
	protected $text;
	protected $time;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_reviews';
		$this->_short_name = 'places_reviews';
		$this->_fields = array("id","place_id","author_name","author_id","profile_photo_url","rating","text","time");
		$this->_required = array("place_id","author_name","author_id","profile_photo_url","rating","text","time");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: author_name -------------------------------------- 

	/** 
	* Sets a value to `author_name` variable
	* @access public
	*/

	public function setAuthorName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('author_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `author_name` variable
	* @access public
	*/

	public function getAuthorName() {
		return $this->author_name;
	}
	
// ------------------------------ End Field: author_name --------------------------------------


// ---------------------------- Start Field: author_id -------------------------------------- 

	/** 
	* Sets a value to `author_id` variable
	* @access public
	*/

	public function setAuthorId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('author_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `author_id` variable
	* @access public
	*/

	public function getAuthorId() {
		return $this->author_id;
	}
	
// ------------------------------ End Field: author_id --------------------------------------


// ---------------------------- Start Field: profile_photo_url -------------------------------------- 

	/** 
	* Sets a value to `profile_photo_url` variable
	* @access public
	*/

	public function setProfilePhotoUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('profile_photo_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `profile_photo_url` variable
	* @access public
	*/

	public function getProfilePhotoUrl() {
		return $this->profile_photo_url;
	}
	
// ------------------------------ End Field: profile_photo_url --------------------------------------


// ---------------------------- Start Field: rating -------------------------------------- 

	/** 
	* Sets a value to `rating` variable
	* @access public
	*/

	public function setRating($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('rating', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `rating` variable
	* @access public
	*/

	public function getRating() {
		return $this->rating;
	}
	
// ------------------------------ End Field: rating --------------------------------------


// ---------------------------- Start Field: text -------------------------------------- 

	/** 
	* Sets a value to `text` variable
	* @access public
	*/

	public function setText($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('text', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `text` variable
	* @access public
	*/

	public function getText() {
		return $this->text;
	}
	
// ------------------------------ End Field: text --------------------------------------


// ---------------------------- Start Field: time -------------------------------------- 

	/** 
	* Sets a value to `time` variable
	* @access public
	*/

	public function setTime($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('time', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `time` variable
	* @access public
	*/

	public function getTime() {
		return $this->time;
	}
	
// ------------------------------ End Field: time --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'author_name' => (object) array(
										'Field'=>'author_name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'author_id' => (object) array(
										'Field'=>'author_id',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'profile_photo_url' => (object) array(
										'Field'=>'profile_photo_url',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'rating' => (object) array(
										'Field'=>'rating',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'text' => (object) array(
										'Field'=>'text',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'time' => (object) array(
										'Field'=>'time',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `places_reviews` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'place_id' => "ALTER TABLE  `places_reviews` ADD  `place_id` varchar(100) NOT NULL   ;",
			'author_name' => "ALTER TABLE  `places_reviews` ADD  `author_name` varchar(100) NOT NULL   ;",
			'author_id' => "ALTER TABLE  `places_reviews` ADD  `author_id` varchar(100) NOT NULL   ;",
			'profile_photo_url' => "ALTER TABLE  `places_reviews` ADD  `profile_photo_url` text NOT NULL   ;",
			'rating' => "ALTER TABLE  `places_reviews` ADD  `rating` int(10) NOT NULL   ;",
			'text' => "ALTER TABLE  `places_reviews` ADD  `text` text NOT NULL   ;",
			'time' => "ALTER TABLE  `places_reviews` ADD  `time` int(10) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_reviews_model.php */
/* Location: ./application/models/Places_reviews_model.php */
