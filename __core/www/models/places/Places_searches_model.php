<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_searches_model Class
 *
 * Manipulates `places_searches` table on database

CREATE TABLE `places_searches` (
  `slug` varchar(150) NOT NULL,
  `name` varchar(100) NOT NULL,
  `times` int(10) NOT NULL DEFAULT '0',
  `lastmod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`slug`)
);

ALTER TABLE  `places_searches` ADD  `slug` varchar(150) NOT NULL   PRIMARY KEY;
ALTER TABLE  `places_searches` ADD  `name` varchar(100) NOT NULL   ;
ALTER TABLE  `places_searches` ADD  `times` int(10) NOT NULL   DEFAULT '0';
ALTER TABLE  `places_searches` ADD  `lastmod` timestamp NOT NULL   DEFAULT 'CURRENT_TIMESTAMP';


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_searches_model extends MY_Model {

	protected $slug;
	protected $name;
	protected $times;
	protected $lastmod;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_searches';
		$this->_short_name = 'places_searches';
		$this->_fields = array("slug","name","times","lastmod");
		$this->_required = array("name","times","lastmod");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

	public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

	public function getSlug() {
		return $this->slug;
	}
	
// ------------------------------ End Field: slug --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: times -------------------------------------- 

	/** 
	* Sets a value to `times` variable
	* @access public
	*/

	public function setTimes($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('times', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `times` variable
	* @access public
	*/

	public function getTimes() {
		return $this->times;
	}
	
// ------------------------------ End Field: times --------------------------------------


// ---------------------------- Start Field: lastmod -------------------------------------- 

	/** 
	* Sets a value to `lastmod` variable
	* @access public
	*/

	public function setLastmod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastmod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `lastmod` variable
	* @access public
	*/

	public function getLastmod() {
		return $this->lastmod;
	}
	
// ------------------------------ End Field: lastmod --------------------------------------



	
	public function get_table_options() {
		return array(
			'slug' => (object) array(
										'Field'=>'slug',
										'Type'=>'varchar(150)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>''
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'times' => (object) array(
										'Field'=>'times',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'lastmod' => (object) array(
										'Field'=>'lastmod',
										'Type'=>'timestamp',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'CURRENT_TIMESTAMP',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'slug' => "ALTER TABLE  `places_searches` ADD  `slug` varchar(150) NOT NULL   PRIMARY KEY;",
			'name' => "ALTER TABLE  `places_searches` ADD  `name` varchar(100) NOT NULL   ;",
			'times' => "ALTER TABLE  `places_searches` ADD  `times` int(10) NOT NULL   DEFAULT '0';",
			'lastmod' => "ALTER TABLE  `places_searches` ADD  `lastmod` timestamp NOT NULL   DEFAULT 'CURRENT_TIMESTAMP';",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_searches_model.php */
/* Location: ./application/models/Places_searches_model.php */
