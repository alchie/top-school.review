<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_claimed_model Class
 *
 * Manipulates `places_claimed` table on database

CREATE TABLE `places_claimed` (
  `place_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `icon` text,
  `rating` decimal(10,5) DEFAULT NULL,
  `url` text,
  `vicinity` text,
  `website` text,
  `international_phone_number` text,
  `formatted_phone_number` text,
  `formatted_address` text,
  `geo_loc_lat` decimal(10,8) DEFAULT NULL,
  `geo_loc_lng` decimal(11,8) DEFAULT NULL,
  `lastmod` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `owner_id` int(20) DEFAULT NULL,
  `user_id` int(20) NOT NULL
);

ALTER TABLE  `places_claimed` ADD  `place_id` varchar(255) NOT NULL   ;
ALTER TABLE  `places_claimed` ADD  `name` varchar(255) NOT NULL   ;
ALTER TABLE  `places_claimed` ADD  `icon` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `rating` decimal(10,5) NULL   ;
ALTER TABLE  `places_claimed` ADD  `url` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `vicinity` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `website` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `international_phone_number` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `formatted_phone_number` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `formatted_address` text NULL   ;
ALTER TABLE  `places_claimed` ADD  `geo_loc_lat` decimal(10,8) NULL   ;
ALTER TABLE  `places_claimed` ADD  `geo_loc_lng` decimal(11,8) NULL   ;
ALTER TABLE  `places_claimed` ADD  `lastmod` timestamp NULL   DEFAULT 'CURRENT_TIMESTAMP';
ALTER TABLE  `places_claimed` ADD  `owner_id` int(20) NULL   ;
ALTER TABLE  `places_claimed` ADD  `user_id` int(20) NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_claimed_model extends MY_Model {

	protected $place_id;
	protected $name;
	protected $icon;
	protected $rating;
	protected $url;
	protected $vicinity;
	protected $website;
	protected $international_phone_number;
	protected $formatted_phone_number;
	protected $formatted_address;
	protected $geo_loc_lat;
	protected $geo_loc_lng;
	protected $lastmod;
	protected $owner_id;
	protected $user_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_claimed';
		$this->_short_name = 'places_claimed';
		$this->_fields = array("place_id","name","icon","rating","url","vicinity","website","international_phone_number","formatted_phone_number","formatted_address","geo_loc_lat","geo_loc_lng","lastmod","owner_id","user_id");
		$this->_required = array("place_id","name","user_id");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: icon -------------------------------------- 

	/** 
	* Sets a value to `icon` variable
	* @access public
	*/

	public function setIcon($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('icon', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `icon` variable
	* @access public
	*/

	public function getIcon() {
		return $this->icon;
	}
	
// ------------------------------ End Field: icon --------------------------------------


// ---------------------------- Start Field: rating -------------------------------------- 

	/** 
	* Sets a value to `rating` variable
	* @access public
	*/

	public function setRating($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('rating', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `rating` variable
	* @access public
	*/

	public function getRating() {
		return $this->rating;
	}
	
// ------------------------------ End Field: rating --------------------------------------


// ---------------------------- Start Field: url -------------------------------------- 

	/** 
	* Sets a value to `url` variable
	* @access public
	*/

	public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `url` variable
	* @access public
	*/

	public function getUrl() {
		return $this->url;
	}
	
// ------------------------------ End Field: url --------------------------------------


// ---------------------------- Start Field: vicinity -------------------------------------- 

	/** 
	* Sets a value to `vicinity` variable
	* @access public
	*/

	public function setVicinity($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('vicinity', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `vicinity` variable
	* @access public
	*/

	public function getVicinity() {
		return $this->vicinity;
	}
	
// ------------------------------ End Field: vicinity --------------------------------------


// ---------------------------- Start Field: website -------------------------------------- 

	/** 
	* Sets a value to `website` variable
	* @access public
	*/

	public function setWebsite($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('website', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `website` variable
	* @access public
	*/

	public function getWebsite() {
		return $this->website;
	}
	
// ------------------------------ End Field: website --------------------------------------


// ---------------------------- Start Field: international_phone_number -------------------------------------- 

	/** 
	* Sets a value to `international_phone_number` variable
	* @access public
	*/

	public function setInternationalPhoneNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('international_phone_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `international_phone_number` variable
	* @access public
	*/

	public function getInternationalPhoneNumber() {
		return $this->international_phone_number;
	}
	
// ------------------------------ End Field: international_phone_number --------------------------------------


// ---------------------------- Start Field: formatted_phone_number -------------------------------------- 

	/** 
	* Sets a value to `formatted_phone_number` variable
	* @access public
	*/

	public function setFormattedPhoneNumber($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('formatted_phone_number', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `formatted_phone_number` variable
	* @access public
	*/

	public function getFormattedPhoneNumber() {
		return $this->formatted_phone_number;
	}
	
// ------------------------------ End Field: formatted_phone_number --------------------------------------


// ---------------------------- Start Field: formatted_address -------------------------------------- 

	/** 
	* Sets a value to `formatted_address` variable
	* @access public
	*/

	public function setFormattedAddress($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('formatted_address', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `formatted_address` variable
	* @access public
	*/

	public function getFormattedAddress() {
		return $this->formatted_address;
	}
	
// ------------------------------ End Field: formatted_address --------------------------------------


// ---------------------------- Start Field: geo_loc_lat -------------------------------------- 

	/** 
	* Sets a value to `geo_loc_lat` variable
	* @access public
	*/

	public function setGeoLocLat($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('geo_loc_lat', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `geo_loc_lat` variable
	* @access public
	*/

	public function getGeoLocLat() {
		return $this->geo_loc_lat;
	}
	
// ------------------------------ End Field: geo_loc_lat --------------------------------------


// ---------------------------- Start Field: geo_loc_lng -------------------------------------- 

	/** 
	* Sets a value to `geo_loc_lng` variable
	* @access public
	*/

	public function setGeoLocLng($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('geo_loc_lng', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `geo_loc_lng` variable
	* @access public
	*/

	public function getGeoLocLng() {
		return $this->geo_loc_lng;
	}
	
// ------------------------------ End Field: geo_loc_lng --------------------------------------


// ---------------------------- Start Field: lastmod -------------------------------------- 

	/** 
	* Sets a value to `lastmod` variable
	* @access public
	*/

	public function setLastmod($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('lastmod', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `lastmod` variable
	* @access public
	*/

	public function getLastmod() {
		return $this->lastmod;
	}
	
// ------------------------------ End Field: lastmod --------------------------------------


// ---------------------------- Start Field: owner_id -------------------------------------- 

	/** 
	* Sets a value to `owner_id` variable
	* @access public
	*/

	public function setOwnerId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('owner_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `owner_id` variable
	* @access public
	*/

	public function getOwnerId() {
		return $this->owner_id;
	}
	
// ------------------------------ End Field: owner_id --------------------------------------


// ---------------------------- Start Field: user_id -------------------------------------- 

	/** 
	* Sets a value to `user_id` variable
	* @access public
	*/

	public function setUserId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('user_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `user_id` variable
	* @access public
	*/

	public function getUserId() {
		return $this->user_id;
	}
	
// ------------------------------ End Field: user_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'varchar(255)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(255)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'icon' => (object) array(
										'Field'=>'icon',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'rating' => (object) array(
										'Field'=>'rating',
										'Type'=>'decimal(10,5)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'url' => (object) array(
										'Field'=>'url',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'vicinity' => (object) array(
										'Field'=>'vicinity',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'website' => (object) array(
										'Field'=>'website',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'international_phone_number' => (object) array(
										'Field'=>'international_phone_number',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'formatted_phone_number' => (object) array(
										'Field'=>'formatted_phone_number',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'formatted_address' => (object) array(
										'Field'=>'formatted_address',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'geo_loc_lat' => (object) array(
										'Field'=>'geo_loc_lat',
										'Type'=>'decimal(10,8)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'geo_loc_lng' => (object) array(
										'Field'=>'geo_loc_lng',
										'Type'=>'decimal(11,8)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'lastmod' => (object) array(
										'Field'=>'lastmod',
										'Type'=>'timestamp',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'CURRENT_TIMESTAMP',
										'Extra'=>''
									),

			'owner_id' => (object) array(
										'Field'=>'owner_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'user_id' => (object) array(
										'Field'=>'user_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'place_id' => "ALTER TABLE  `places_claimed` ADD  `place_id` varchar(255) NOT NULL   ;",
			'name' => "ALTER TABLE  `places_claimed` ADD  `name` varchar(255) NOT NULL   ;",
			'icon' => "ALTER TABLE  `places_claimed` ADD  `icon` text NULL   ;",
			'rating' => "ALTER TABLE  `places_claimed` ADD  `rating` decimal(10,5) NULL   ;",
			'url' => "ALTER TABLE  `places_claimed` ADD  `url` text NULL   ;",
			'vicinity' => "ALTER TABLE  `places_claimed` ADD  `vicinity` text NULL   ;",
			'website' => "ALTER TABLE  `places_claimed` ADD  `website` text NULL   ;",
			'international_phone_number' => "ALTER TABLE  `places_claimed` ADD  `international_phone_number` text NULL   ;",
			'formatted_phone_number' => "ALTER TABLE  `places_claimed` ADD  `formatted_phone_number` text NULL   ;",
			'formatted_address' => "ALTER TABLE  `places_claimed` ADD  `formatted_address` text NULL   ;",
			'geo_loc_lat' => "ALTER TABLE  `places_claimed` ADD  `geo_loc_lat` decimal(10,8) NULL   ;",
			'geo_loc_lng' => "ALTER TABLE  `places_claimed` ADD  `geo_loc_lng` decimal(11,8) NULL   ;",
			'lastmod' => "ALTER TABLE  `places_claimed` ADD  `lastmod` timestamp NULL   DEFAULT 'CURRENT_TIMESTAMP';",
			'owner_id' => "ALTER TABLE  `places_claimed` ADD  `owner_id` int(20) NULL   ;",
			'user_id' => "ALTER TABLE  `places_claimed` ADD  `user_id` int(20) NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_claimed_model.php */
/* Location: ./application/models/Places_claimed_model.php */
