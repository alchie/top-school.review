<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_media_model Class
 *
 * Manipulates `places_media` table on database

CREATE TABLE `places_media` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(255) NOT NULL,
  `media_type` varchar(100) NOT NULL,
  `media_key` varchar(200) NOT NULL,
  `media_value` text,
  `media_title` varchar(200) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `claim_id` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `place_id` (`place_id`)
);

ALTER TABLE  `places_media` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `places_media` ADD  `place_id` varchar(255) NOT NULL   ;
ALTER TABLE  `places_media` ADD  `media_type` varchar(100) NOT NULL   ;
ALTER TABLE  `places_media` ADD  `media_key` varchar(200) NOT NULL   ;
ALTER TABLE  `places_media` ADD  `media_value` text NULL   ;
ALTER TABLE  `places_media` ADD  `media_title` varchar(200) NULL   ;
ALTER TABLE  `places_media` ADD  `active` int(1) NOT NULL   DEFAULT '1';
ALTER TABLE  `places_media` ADD  `claim_id` int(20) NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_media_model extends MY_Model {

	protected $id;
	protected $place_id;
	protected $media_type;
	protected $media_key;
	protected $media_value;
	protected $media_title;
	protected $active;
	protected $claim_id;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_media';
		$this->_short_name = 'places_media';
		$this->_fields = array("id","place_id","media_type","media_key","media_value","media_title","active","claim_id");
		$this->_required = array("place_id","media_type","media_key","active");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: media_type -------------------------------------- 

	/** 
	* Sets a value to `media_type` variable
	* @access public
	*/

	public function setMediaType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('media_type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `media_type` variable
	* @access public
	*/

	public function getMediaType() {
		return $this->media_type;
	}
	
// ------------------------------ End Field: media_type --------------------------------------


// ---------------------------- Start Field: media_key -------------------------------------- 

	/** 
	* Sets a value to `media_key` variable
	* @access public
	*/

	public function setMediaKey($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('media_key', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `media_key` variable
	* @access public
	*/

	public function getMediaKey() {
		return $this->media_key;
	}
	
// ------------------------------ End Field: media_key --------------------------------------


// ---------------------------- Start Field: media_value -------------------------------------- 

	/** 
	* Sets a value to `media_value` variable
	* @access public
	*/

	public function setMediaValue($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('media_value', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `media_value` variable
	* @access public
	*/

	public function getMediaValue() {
		return $this->media_value;
	}
	
// ------------------------------ End Field: media_value --------------------------------------


// ---------------------------- Start Field: media_title -------------------------------------- 

	/** 
	* Sets a value to `media_title` variable
	* @access public
	*/

	public function setMediaTitle($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('media_title', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `media_title` variable
	* @access public
	*/

	public function getMediaTitle() {
		return $this->media_title;
	}
	
// ------------------------------ End Field: media_title --------------------------------------


// ---------------------------- Start Field: active -------------------------------------- 

	/** 
	* Sets a value to `active` variable
	* @access public
	*/

	public function setActive($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('active', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `active` variable
	* @access public
	*/

	public function getActive() {
		return $this->active;
	}
	
// ------------------------------ End Field: active --------------------------------------


// ---------------------------- Start Field: claim_id -------------------------------------- 

	/** 
	* Sets a value to `claim_id` variable
	* @access public
	*/

	public function setClaimId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('claim_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `claim_id` variable
	* @access public
	*/

	public function getClaimId() {
		return $this->claim_id;
	}
	
// ------------------------------ End Field: claim_id --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'varchar(255)',
										'Null'=>'NO',
										'Key'=>'MUL',
										'Default'=>'',
										'Extra'=>''
									),

			'media_type' => (object) array(
										'Field'=>'media_type',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'media_key' => (object) array(
										'Field'=>'media_key',
										'Type'=>'varchar(200)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'media_value' => (object) array(
										'Field'=>'media_value',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'media_title' => (object) array(
										'Field'=>'media_title',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'active' => (object) array(
										'Field'=>'active',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'1',
										'Extra'=>''
									),

			'claim_id' => (object) array(
										'Field'=>'claim_id',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `places_media` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'place_id' => "ALTER TABLE  `places_media` ADD  `place_id` varchar(255) NOT NULL   ;",
			'media_type' => "ALTER TABLE  `places_media` ADD  `media_type` varchar(100) NOT NULL   ;",
			'media_key' => "ALTER TABLE  `places_media` ADD  `media_key` varchar(200) NOT NULL   ;",
			'media_value' => "ALTER TABLE  `places_media` ADD  `media_value` text NULL   ;",
			'media_title' => "ALTER TABLE  `places_media` ADD  `media_title` varchar(200) NULL   ;",
			'active' => "ALTER TABLE  `places_media` ADD  `active` int(1) NOT NULL   DEFAULT '1';",
			'claim_id' => "ALTER TABLE  `places_media` ADD  `claim_id` int(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_media_model.php */
/* Location: ./application/models/Places_media_model.php */
