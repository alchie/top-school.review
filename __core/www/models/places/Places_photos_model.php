<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_photos_model Class
 *
 * Manipulates `places_photos` table on database

CREATE TABLE `places_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_id` varchar(100) NOT NULL,
  `photo_reference` text NOT NULL,
  `height` int(10) NOT NULL,
  `width` int(10) NOT NULL,
  `contributor` varchar(100) NOT NULL,
  `contrib_url` text NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `places_photos` ADD  `id` int(11) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `places_photos` ADD  `place_id` varchar(100) NOT NULL   ;
ALTER TABLE  `places_photos` ADD  `photo_reference` text NOT NULL   ;
ALTER TABLE  `places_photos` ADD  `height` int(10) NOT NULL   ;
ALTER TABLE  `places_photos` ADD  `width` int(10) NOT NULL   ;
ALTER TABLE  `places_photos` ADD  `contributor` varchar(100) NOT NULL   ;
ALTER TABLE  `places_photos` ADD  `contrib_url` text NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_photos_model extends MY_Model {

	protected $id;
	protected $place_id;
	protected $photo_reference;
	protected $height;
	protected $width;
	protected $contributor;
	protected $contrib_url;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_photos';
		$this->_short_name = 'places_photos';
		$this->_fields = array("id","place_id","photo_reference","height","width","contributor","contrib_url");
		$this->_required = array("place_id","photo_reference","height","width","contributor","contrib_url");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: photo_reference -------------------------------------- 

	/** 
	* Sets a value to `photo_reference` variable
	* @access public
	*/

	public function setPhotoReference($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('photo_reference', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `photo_reference` variable
	* @access public
	*/

	public function getPhotoReference() {
		return $this->photo_reference;
	}
	
// ------------------------------ End Field: photo_reference --------------------------------------


// ---------------------------- Start Field: height -------------------------------------- 

	/** 
	* Sets a value to `height` variable
	* @access public
	*/

	public function setHeight($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('height', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `height` variable
	* @access public
	*/

	public function getHeight() {
		return $this->height;
	}
	
// ------------------------------ End Field: height --------------------------------------


// ---------------------------- Start Field: width -------------------------------------- 

	/** 
	* Sets a value to `width` variable
	* @access public
	*/

	public function setWidth($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('width', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `width` variable
	* @access public
	*/

	public function getWidth() {
		return $this->width;
	}
	
// ------------------------------ End Field: width --------------------------------------


// ---------------------------- Start Field: contributor -------------------------------------- 

	/** 
	* Sets a value to `contributor` variable
	* @access public
	*/

	public function setContributor($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('contributor', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `contributor` variable
	* @access public
	*/

	public function getContributor() {
		return $this->contributor;
	}
	
// ------------------------------ End Field: contributor --------------------------------------


// ---------------------------- Start Field: contrib_url -------------------------------------- 

	/** 
	* Sets a value to `contrib_url` variable
	* @access public
	*/

	public function setContribUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('contrib_url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `contrib_url` variable
	* @access public
	*/

	public function getContribUrl() {
		return $this->contrib_url;
	}
	
// ------------------------------ End Field: contrib_url --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(11)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'photo_reference' => (object) array(
										'Field'=>'photo_reference',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'height' => (object) array(
										'Field'=>'height',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'width' => (object) array(
										'Field'=>'width',
										'Type'=>'int(10)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'contributor' => (object) array(
										'Field'=>'contributor',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'contrib_url' => (object) array(
										'Field'=>'contrib_url',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `places_photos` ADD  `id` int(11) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'place_id' => "ALTER TABLE  `places_photos` ADD  `place_id` varchar(100) NOT NULL   ;",
			'photo_reference' => "ALTER TABLE  `places_photos` ADD  `photo_reference` text NOT NULL   ;",
			'height' => "ALTER TABLE  `places_photos` ADD  `height` int(10) NOT NULL   ;",
			'width' => "ALTER TABLE  `places_photos` ADD  `width` int(10) NOT NULL   ;",
			'contributor' => "ALTER TABLE  `places_photos` ADD  `contributor` varchar(100) NOT NULL   ;",
			'contrib_url' => "ALTER TABLE  `places_photos` ADD  `contrib_url` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_photos_model.php */
/* Location: ./application/models/Places_photos_model.php */
