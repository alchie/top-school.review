<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Counties_model Class
 *
 * Manipulates `counties` table on database

CREATE TABLE `counties` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `state_id` int(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE  `counties` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `counties` ADD  `state_id` int(20) NOT NULL   ;
ALTER TABLE  `counties` ADD  `name` varchar(100) NOT NULL   ;
ALTER TABLE  `counties` ADD  `slug` varchar(200) NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Counties_model extends MY_Model {

	protected $id;
	protected $state_id;
	protected $name;
	protected $slug;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'counties';
		$this->_short_name = 'counties';
		$this->_fields = array("id","state_id","name","slug");
		$this->_required = array("state_id","name");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: id -------------------------------------- 

	/** 
	* Sets a value to `id` variable
	* @access public
	*/

	public function setId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `id` variable
	* @access public
	*/

	public function getId() {
		return $this->id;
	}
	
// ------------------------------ End Field: id --------------------------------------


// ---------------------------- Start Field: state_id -------------------------------------- 

	/** 
	* Sets a value to `state_id` variable
	* @access public
	*/

	public function setStateId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('state_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `state_id` variable
	* @access public
	*/

	public function getStateId() {
		return $this->state_id;
	}
	
// ------------------------------ End Field: state_id --------------------------------------


// ---------------------------- Start Field: name -------------------------------------- 

	/** 
	* Sets a value to `name` variable
	* @access public
	*/

	public function setName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `name` variable
	* @access public
	*/

	public function getName() {
		return $this->name;
	}
	
// ------------------------------ End Field: name --------------------------------------


// ---------------------------- Start Field: slug -------------------------------------- 

	/** 
	* Sets a value to `slug` variable
	* @access public
	*/

	public function setSlug($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('slug', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `slug` variable
	* @access public
	*/

	public function getSlug() {
		return $this->slug;
	}
	
// ------------------------------ End Field: slug --------------------------------------



	
	public function get_table_options() {
		return array(
			'id' => (object) array(
										'Field'=>'id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'state_id' => (object) array(
										'Field'=>'state_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'name' => (object) array(
										'Field'=>'name',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'slug' => (object) array(
										'Field'=>'slug',
										'Type'=>'varchar(200)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'id' => "ALTER TABLE  `counties` ADD  `id` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'state_id' => "ALTER TABLE  `counties` ADD  `state_id` int(20) NOT NULL   ;",
			'name' => "ALTER TABLE  `counties` ADD  `name` varchar(100) NOT NULL   ;",
			'slug' => "ALTER TABLE  `counties` ADD  `slug` varchar(200) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Counties_model.php */
/* Location: ./application/models/Counties_model.php */
