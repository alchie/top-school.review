<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

        var $db_configs = array();
        var $template_folder = '';

        public function __construct()
        {
                parent::__construct();

                $this->template_data->set('app_root', 'www');
                $this->template_data->set('app_id', 'www');
                $this->template_data->set('app_name', 'Search');
                $this->template_data->set('template_folder', $this->template_folder);

                $this->template_data->set('page_title', APP_DEFAULT_TITLE);
                $this->template_data->set('meta_description', APP_DEFAULT_DESCRIPTION);
                $this->template_data->set('meta_keywords', APP_DEFAULT_KEYWORDS);
                $this->template_data->set('google_analytics_tracking_id', $this->config->item('general_gat_id'));
                $this->template_data->set('canonical_url', site_url( uri_string() ) );

                $this->template_data->set('site_title', APP_DEFAULT_TITLE);
                $this->template_data->set('site_url', site_url() );
                $this->template_data->set('site_logo', 'https://1.bp.blogspot.com/-eBCeOafE1Ko/WgxUUFyIrRI/AAAAAAAAN0w/uGshRrzXeDI7W5Gb2UgHxctA8gKpTy-XgCLcBGAs/s1600/trokis.png' );

                $this->db_configs = $this->config->item('db_configs');

                $this->template_data->set('facebook_app_id', $this->config->item('fbAppId'));

                if( $this->input->get('ref') ) {
                    set_referral_id($this->input->get('ref'));
                }
                
        }


        public function _load_models($id) {

            $models = array();
            
            switch($id) {
                case 'account':
                case 'all':
                    $models[] = 'account/Users_model'; 
                    $models[] = 'account/Facebook_accounts_model';
                    $models[] = 'account/Google_accounts_model';
                    $models[] = 'account/Recovery_codes_model';
                break;
                case 'places':
                    $models[] = 'places/Categories_model';
                    $models[] = 'places/Places_category_model';
                    $models[] = 'places/Places_claimed_model';
                    $models[] = 'places/Places_data_model';
                    $models[] = 'places/Places_keyword_model';
                    $models[] = 'places/Places_location_model';
                    $models[] = 'places/Places_media_model';
                    $models[] = 'places/Places_meta_model';
                    $models[] = 'places/Places_redirect_model';
                    $models[] = 'places/Places_searches_model';
                    $models[] = 'places/Places_type_model';
                break;
                case 'sessions':
                    // ./sessions:
                    $models[] = 'sessions/App_sessions_model'; 
                    $models[] = 'sessions/Ci_sessions_model'; 
                    $models[] = 'sessions/User_sessions_model'; 
                break;
                case 'common':
                    // ./common
                    $models[] = 'common/Cities_model';
                    $models[] = 'common/Counties_model';
                    $models[] = 'common/States_model';
                    $models[] = 'common/Terms_model';
                break;
            }
            $this->load->model($models);
        }

        public function check_login_status($url=NULL) {
            $url = ($url && $url != '') ? $url : account_url( "profile" );
            if( $this->session->userdata('loggedIn') ) {
                redirect( $url );
            }
        }

        public function must_login($uri='login') {
            if( !$this->session->userdata('loggedIn') ) {
                $this->session->sess_destroy();
                redirect( account_url( $uri ) );
            }
        }

        public function must_admin($uri='login') {
                if( $this->session->userdata('type') != 'admin' ) {
                    redirect( account_url( $uri ) );
                }
        }

        public function page_not_found() {
            $this->load->view('page_not_found', $this->template_data->get_data());
        }

        protected function _must_login($uri='login', $return='') {
            if( !$this->session->userdata('loggedIn') ) {
                $this->session->sess_destroy();
                redirect( account_url( $uri ) . "?app=" . $this->template_data->get('app_id') . "&uri=" . $return );
            }
        }

}

class PLACES_Controller extends MY_Controller {

        public function __construct()
        {

            parent::__construct();
            $this->_load_models('places');
            $this->_load_models('common');

            $this->template_data->set('app_root', 'places');
            $this->template_data->set('app_id', 'places');
            $this->template_data->set('app_name', 'Places');

            $this->template_data->set('canonical_url', site_url( uri_string() ) );

        }

        public function get_location_by_city($city_id) {
            $city = new Cities_model('c', 'common');
            $city->setId($city_id,true);
            $city->set_select("c.*");
            $city->set_select("c.name as city");
            $city->set_select("c.slug as city_slug");
            $city->set_select("(SELECT cc.name FROM counties cc WHERE cc.id=c.county_id) as county");
            $city->set_select("(SELECT cc.slug FROM counties cc WHERE cc.id=c.county_id) as county_slug");
            $city->set_select("(SELECT s.name FROM states s WHERE s.id=c.state_id) as state");
            $city->set_select("(SELECT s.slug FROM states s WHERE s.id=c.state_id) as state_slug");
            $city_data = $city->get();
            return $city_data;
        }

}

class SESSIONS_Controller extends CI_Controller {
    
    public $app_id = 'accounts';

    public function __construct() {
        parent::__construct();
    }

    public function index() {       
        redirect( account_url("login") . "?app=" . $this->app_id );
    }

    public function start($session_id) {
        $this->load->model('sessions/App_sessions_model');
        $session = new $this->App_sessions_model(NULL, 'default');
        $session->setAppId($this->app_id,true);
        $session->setSessionId($session_id,true);
        if( $session->nonEmpty() ) {
            $session_results = $session->get_results(); 
            $this->load->model('account/Users_model');
            $user = new $this->Users_model(NULL, 'account');
            $user->setUid($session_results->user_id,true);
            if( $user->nonEmpty() ) {
                $results = $user->get_results(); 
                $this->session->set_userdata( 'loggedIn', true );
                $this->session->set_userdata( 'userId', $results->uid );
                $this->session->set_userdata( 'fullName', $results->full_name );
                $this->session->set_userdata( 'email', $results->email );
                $this->session->set_userdata( 'type', $results->type );
                $this->session->set_userdata( 'dateRegistered', $results->date_registered );
                $this->session->set_userdata( 'emailVerified', (bool) $results->verified );
                $this->add_user_session($results->uid, session_id() );
            }
            $session->delete();
            $app = ($this->input->get('app_id')) ? $this->input->get('app_id') : $this->app_id;
            
        } 
        $this->_redirect( $app , $this->input->get('uri') );
    }

    protected function add_user_session($user_id, $session_id) {
        $this->load->model('sessions/User_sessions_model');
        $session = new $this->User_sessions_model(NULL, 'default');
        $session->setUserId($user_id,true);
        $session->setSessionId($session_id,true);
        if( !$session->nonEmpty() ) {
            $session->insert();
        }
    }

    protected function _redirect($app_id='', $uri=NULL) {
        $app_id = ($this->session->userdata('redirect_to')) ? $this->session->userdata('redirect_to') : $app_id;
        $uri = ($this->session->userdata('redirect_to_uri')) ? $this->session->userdata('redirect_to_uri') : $uri;
        switch($app_id) {
            case 'accounts':
                $redirect_url = account_url("my-account");
            break;
            case 'realestate':
                $redirect_url = realestate_url($uri);
            break;
            case 'motorcycles':
                $redirect_url = motorcycles_url($uri);
            break;
            case 'vehicles':
                $redirect_url = vehicles_url($uri);
            break;
            case 'motorcycles':
                $redirect_url = motorcycles_url($uri);
            break;
            case 'events':
                $redirect_url = events_url($uri);
            break;
            case 'jobs':
                $redirect_url = jobs_url($uri);
            break;
            case 'schools':
                $redirect_url = schools_url($uri);
            break;
            case 'companies':
                $redirect_url = site_url($uri);
            break;
            case 'howto':
                $redirect_url = howto_url($uri);
            break;
            case 'places':
                $redirect_url = places_url($uri);
            break;
            case 'software':
                $redirect_url = software_url($uri);
            break;
            case 'www':
            default:
                $redirect_url = main_url($uri);
            break;
        }
        redirect( $redirect_url  );
    }
}