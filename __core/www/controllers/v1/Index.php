<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends PLACES_Controller {
	
	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {	

		$places = new $this->Places_data_model('c', 'places');
		$places->cache_on();
		$places->set_start( $start );
		$places->set_limit( 3 );
		$places->set_select('c.*');
		$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');

		$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
		$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');
		
		$places->setDone(1,true);
		$places->set_where('c.rating > 4.5');
		$places->set_order('RAND()');
		$this->template_data->set('places', $places->populate());

		$this->load->view('index/index', $this->template_data->get_data());
	}

}
