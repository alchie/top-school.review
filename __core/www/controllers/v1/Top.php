<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Top extends PLACES_Controller {
	
	var $list_limit = 12;

	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {
		$places = new $this->Places_data_model('c', 'places');
		$places->cache_on();
		$places->set_order('c.lastmod', 'DESC');
		$places->set_start( $start );
		$places->set_limit( $this->list_limit );
		$places->set_select('c.*');
		$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
		$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
		$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

		$this->template_data->set('places', $places->populate());
		$this->template_data->set('places_all', $places->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 2,
			'base_url' => base_url( $this->config->item('index_page') . "/directory/"),
			'total_rows' => $places->count_all_results(),
			'per_page' => $places->get_limit()
		)));

		$this->load->view('top/index', $this->template_data->get_data());
	}

	public function category($category, $start=0) {
		$places = new $this->Places_data_model('c', 'places');
		$places->cache_on();
		$places->set_order('c.lastmod', 'DESC');
		$places->set_start( $start );
		$places->set_limit( $this->list_limit );
		$places->set_select('c.*');
		$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
		$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
		$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

		$this->template_data->set('places', $places->populate());
		$this->template_data->set('places_all', $places->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 2,
			'base_url' => base_url( $this->config->item('index_page') . "/directory/"),
			'total_rows' => $places->count_all_results(),
			'per_page' => $places->get_limit()
		)));

		$this->load->view('top/category', $this->template_data->get_data());
	}

}
