<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View extends PLACES_Controller {
	
	public function __construct() {
		parent::__construct();

	}

	private function _view($slug) {

		$place = new $this->Places_data_model('c', 'places');
		$place->setSlug($slug,true);
		$place->set_select("c.*");
		$place->cache_on();

		if( $place->nonEmpty() ) {
			
			$place->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');

			$place->set_select('(SELECT cr.url FROM places_redirect cr WHERE cr.place_id=c.place_id LIMIT 1) as redirect_url');

			$place->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
			$place->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

			$place_data = $place->get();
			$this->template_data->set('current_place', $place_data);

			$this->template_data->set('location', $this->get_location_by_city($place_data->city_id) );


			$this->template_data->set('redirect_url', $place_data->redirect_url);
			$id = $place_data->place_id;

			$this->template_data->set('page_title', $place_data->name );
	    	$this->template_data->set('meta_description', "{$place_data->name} ({$place_data->formatted_phone_number}) is located at {$place_data->vicinity}.");
	    	$this->template_data->set('meta_keywords', "{$place_data->name}, {$place_data->vicinity}");

			$this->template_data->set('amphtml_url', site_url("amp_{$place_data->slug}") );

			$this->template_data->opengraph(array(
				'fb:app_id'=>$this->config->item('fbAppId'),
				'og:image' => ((isset($place_data->logo_url)) && ($place_data->logo_url)) ? $place_data->logo_url : $this->config->item('trokis_logo_url'),
				'og:description' => $this->template_data->get('meta_description'),
				'og:site_name' => 'Trokis Philippines',
				'og:title' => $place_data->name,
			));

			return true;
				
		} else {
			return false;
		}
	}

	private function _others($except, $limit=3) {
		$places = new $this->Places_data_model('c', 'places');
		$places->cache_on();
		$places->set_start( 0 );
		$places->set_limit( $limit );
		$places->set_select('c.*');
		$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');

		$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
		$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');
		
		$places->setDone(1,true);
		$places->set_order('RAND()');
		$places->set_where('c.id != '. $except);
		$this->template_data->set('other_places', $places->populate());
	}

	public function by_slug($slug) {

		if( $this->_view($slug) ) {

			$place_data = $this->template_data->get('current_place');

			$this->_others($place_data->id, 5);

			$this->load->view('view/info', $this->template_data->get_data());

			
		} else {
			$this->load->view('page_not_found', $this->template_data->get_data());
		}
	}

	public function amp_by_slug($slug) {
		if( $this->_view($slug) ) {
			$this->load->view('view/amp', $this->template_data->get_data());
		} else {
			$this->load->view('page_not_found', $this->template_data->get_data());
		}
	}


	private function _media_actions($place_data, $mtype='image', $mkey='gallery') {
		$submitted = new $this->Places_media_model('cm', 'places');
		$submitted->setPlaceId($place_data->id,true);
		$submitted->setMediaType($mtype,true);
		//$submitted->setMediaKey($mkey,true);
		$submitted->set_where_in('cm.media_key',$mkey);
		$submitted->setActive(1,true);
		$submitted->cache_on();
		$this->template_data->set('images', $submitted->populate());

		if( $this->session->userdata('loggedIn') ) {
		if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) {

			$unapproved = new $this->Places_media_model('cm', 'places');
			$unapproved->setPlaceId($place_data->id,true);
			$unapproved->setMediaType($mtype,true);
			//$unapproved->setMediaKey($mkey,true);
			$unapproved->set_where_in('cm.media_key',$mkey);
			$unapproved->setActive(0,true);
			$this->template_data->set('unapproved', $unapproved->populate());

		}}


	}

	private function _media_data($media_id) {
		$current_media = new $this->Places_media_model(NULL, 'places');
		$current_media->setId($media_id,true);

		if( $this->session->userdata('loggedIn') ) {
		if( ($this->session->userdata('type')!==null) && ($this->session->userdata('type') == 'admin') ) {
			if( $this->input->get('approve') ) {
				$current_media->setActive(1,false,true);
				$current_media->update();
				redirect( site_url($this->input->get('next')) );
			}
			if( $this->input->get('delete') ) {
				$current_media->delete();
				redirect( site_url($this->input->get('next')) );
			}
		} else {
			$current_media->cache_on();
		}}

		$this->template_data->set('current_media', $current_media->get());
	}

	private function _contact_action($place_data) {
		if( $this->input->post() ) {
		$to = "company{$place_data->id}@trokis.com";
		$full_name = strip_tags($this->input->post("full_name",true));
		$phone_number = strip_tags($this->input->post("phone_number",true));
		$email_address = strip_tags($this->input->post("email_address",true));
		$message = strip_tags($this->input->post("message",true));
		$message_body = "FULL NAME: {$full_name}\n";
		$message_body .= "PHONE NUMBER: {$phone_number}\n";
		$message_body .= "EMAIL ADDRESS: {$email_address}\n";
		$message_body .= "- - - - - - - - - - - - - - - - - - - - - \n\n";
		$message_body .= "{$message}\n\n";
		$message_body .= "SOURCE: " . site_url($place_data->slug);
		$message_body = urlencode($message_body);
		$gmail_link = "https://mail.google.com/mail/?view=cm&fs=1&to={$to}&su=TROKIS.COM&body={$message_body}";
		redirect( $gmail_link );

		}

	}

}
