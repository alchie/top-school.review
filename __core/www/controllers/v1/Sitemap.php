<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends PLACES_Controller {

	var $limit = 1000;

	public function __construct() {
		parent::__construct();
		$this->output->set_content_type('application/xml');
		$this->template_data->set('limit', $this->limit);
	}

	public function index() {

		$places = new $this->Places_data_model('c', 'places');
		$this->template_data->set('places', $places->count_all_results());

		$this->load->view('places/sitemap_list', $this->template_data->get_data());
	}

	public function page($page=1) {

		$places = new $this->Places_data_model('c', 'places');
		$places->set_limit($this->limit);
		$places->set_start(($page-1)*$this->limit);
		$places->set_order('c.lastmod', 'ASC');
		$this->template_data->set('places', $places->populate());

		$this->load->view('places/sitemap_content', $this->template_data->get_data());
	}

	public function places($page=1) {

		$places = new $this->Places_data_model('c', 'places');
		$places->set_limit($this->limit);
		$places->set_start(($page-1)*$this->limit);
		$places->set_order('c.lastmod', 'ASC');
		$this->template_data->set('places', $places->populate());

		$this->load->view('places/sitemap_content', $this->template_data->get_data());
	}

	public function wildcards($page=1) {

		$places = new $this->Places_data_model('c', 'places');
		$places->set_limit($this->limit);
		$places->set_start(($page-1)*$this->limit);
		$places->set_order('c.lastmod', 'ASC');
		$this->template_data->set('places', $places->populate());

		$this->load->view('places/sitemap_wildcard', $this->template_data->get_data());
	}


	public function locations() {

		$states = new $this->States_model('s', 'common');
        $states->set_limit(0);
        $this->template_data->set('locations', $states->populate());

        $this->template_data->set('uri', 'state');

		$this->load->view('places/sitemap_list_locations', $this->template_data->get_data());
	}

	public function location_state($state_slug) {

		$states = new $this->States_model('s', 'common');
		$states->setSlug($state_slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$counties = new $this->Counties_model('s', 'common');
			$counties->setStateId($state->id,true);
        	$counties->set_limit(0);
       		$this->template_data->set('locations', $counties->populate());

       	 	$this->template_data->set('uri', 'county');

			$this->load->view('places/sitemap_list_locations', $this->template_data->get_data());
		} else {
			$this->page_not_found();
		}
	}

	public function location_county($county_slug) {

		$counties = new $this->Counties_model('s', 'common');
		$counties->setSlug($county_slug,true);
		
		if( $counties->nonEmpty() ) {
			$county = $counties->get_results();

			$cities = new $this->Cities_model('s', 'common');
			$cities->setCountyId($county->id,true);
        	$cities->set_limit(0);
       		$this->template_data->set('locations', $cities->populate());

       	 	$this->template_data->set('uri', 'city');

			$this->load->view('places/sitemap_list_locations', $this->template_data->get_data());
		} else {
			$this->page_not_found();
		}
	}

	public function location_city($city_slug, $page=1) {

		$cities = new $this->Cities_model('s', 'common');
		$cities->setSlug($city_slug,true);
		
		if( $cities->nonEmpty() ) {
			$city = $cities->get_results();

			$places = new $this->Places_data_model('c', 'places');
			$places->setCityId($city->id, true);
			$places->set_limit(0);
			$places->set_order('c.lastmod', 'ASC');
			$this->template_data->set('places', $places->populate());

			$this->load->view('places/sitemap_content', $this->template_data->get_data());
		} else {
			$this->page_not_found();
		}
	}

	public function by_location($slug, $page=1) {

		$current_location = $this->_get_location($slug);
		
		$places = new $this->Places_location_model('l', 'places');
		$places->cache_on();
		$places->setLocationId($current_location->location_id,true);
		$places->set_order('c.lastmod', 'DESC');
		$places->set_start(($page-1)*$this->limit);
		$places->set_limit( $this->limit );
		$places->set_select('c.*');
		$places->set_join("places_data c", 'c.id=l.place_id');
		$this->template_data->set('places', $places->populate());

		$this->load->view('places/sitemap_content', $this->template_data->get_data());
	}

	public function searches() {

		$search = new $this->Places_searches_model('s', 'places');
		//$search->set_order('lastmod', 'DESC');
		$this->template_data->set('total_count', $search->count_all());

		$this->load->view('places/sitemap_list_searches', $this->template_data->get_data());
	}

	public function searches_items($page=1) {
		$search = new $this->Places_searches_model('s', 'places');
		$search->set_order('lastmod', 'DESC');
		$search->set_limit($this->limit);
		$search->set_start( ($page-1) * $this->limit );
		$this->template_data->set('search_items', $search->populate());

		$this->load->view('places/sitemap_content_searches', $this->template_data->get_data());
	}

}
