<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	
	var $list_limit = 12;

	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {	
		$this->load->view('login/index', $this->template_data->get_data());
	}

    public function api($api=NULL) {
        switch($api) {
            case 'google':
                $this->load->library('google');
                redirect( $this->google->login_url() );
            break;
        }
        redirect( "login" );
    }

    public function redirect($api) {
        switch($api) {
            case 'facebook':
                $this->load->library('facebook');
                if( $this->facebook->is_authenticated() )
                {
                    $user = $this->facebook->request('get', '/me?fields=id,name,email');
                    if (!isset($user['error']))
                    {
                        $fb = new $this->Facebook_accounts_model(NULL, 'account');
                        $fb->setFacebookId($user['id'],true);
                        $fb->setName($user['name']);
                        $fb->setEmail($user['email']);
                        if( $fb->nonEmpty() ) {
                            $result = $fb->get_results();
                            if($result->user_id) {
                                $user = new $this->Users_model(NULL, 'account');
                                $user->setUid($result->user_id,true);
                                if( $user->nonEmpty() ) {
                                    $user_results = $user->get_results(); 
                                    $this->__start_user_session($user_results);
                                    $app_id = $this->input->post( 'app_id',true );
                                    $this->add_user_session($result->user_id, session_id() );
                                    $this->app_redirect( $app_id, session_id() );
                                } else {
                                    redirect( account_url("register/api/facebook") );
                                }
                                
                            } else {
                                if( $user_session = $this->__start_session_by_email($user['email']) ) {
                                    $fb->setUserId($user_session->uid,false,true);
                                    $fb->update();
                                    redirect( account_url("my-account") );
                                } else {
                                    redirect( account_url("register/api/facebook") );
                                }
                            }
                        } else {
                            $fb->insert();
                            if( $user_session = $this->__start_session_by_email($user['email']) ) {
                                $fb->setUserId($user_session->uid,false,true);
                                $fb->update();
                                redirect( account_url("my-account") );
                            } else {
                                redirect( account_url("register/api/facebook") );
                            }
                        }
                    }

                } else {
                    redirect( account_url() );
                }
            break;
            case 'google':
                $this->load->library('google');
                if ($this->google->is_authenticated())
                {
                    $user = $this->google->getUser();
                    if ($user) {
                        $ga = new $this->Google_accounts_model(NULL, 'account');
                        $ga->setGoogleId($user->id,true);
                        $ga->setName($user->name);
                        $ga->setEmail($user->email);
                        if( $ga->nonEmpty() ) {
                            $result = $ga->get_results();
                            if($result->user_id) {

                                $user = new $this->Users_model(NULL, 'account');
                                $user->setUid($result->user_id,true);
                                
                                if( $user->nonEmpty() ) {
                                    $user_results = $user->get_results(); 
                                    $this->__start_user_session($user_results);
                                    $app_id = $this->input->post( 'app_id',true );
                                    $this->add_user_session($result->user_id, session_id() );
                                    $this->app_redirect( $app_id, session_id() );
                                    redirect( account_url("my-account") );
                                } else {
                                    redirect( account_url("register/api/google") );
                                }
                                
                            } else {
                                if( $user_session = $this->__start_session_by_email($user->email) ) {
                                    $ga->setUserId($user_session->uid,false,true);
                                    $ga->update();
                                    redirect( account_url("my-account") );
                                } else {
                                    redirect( account_url("register/api/google") );
                                }
                            }
                        } else {
                            $ga->insert();
                            if( $user_session = $this->__start_session_by_email($user->email) ) {
                                $ga->setUserId($user_session->uid,false,true);
                                $ga->update();
                                redirect( account_url("my-account") );
                            } else {
                                redirect( account_url("register/api/google") );
                            }
                        }
                    }
                }else {
                    redirect( account_url() );
                }
            break;
        }
    }

    public function connect($api) {
        switch($api) {
            case 'facebook':
                $this->load->library('facebook');
                if ($this->facebook->is_authenticated())
                {
                    $user = $this->facebook->request('get', '/me?fields=id,name,email');
                    if (!isset($user['error']))
                    {
                        $fb = new $this->Facebook_accounts_model(NULL, 'account');
                        $fb->setFacebookId($user['id'],true);
                        $fb->setName($user['name']);
                        $fb->setEmail($user['email']);
                        $fb->setUserId($this->session->userId);
                        if( !$fb->nonEmpty() ) {
                            $fb->insert();
                        }
                    }
                }
                redirect( account_url("my-account-facebook") );
            break;
            case 'google':
                $this->load->library('google');
                if ($this->google->is_authenticated())
                {
                    $user = $this->google->getUser();
                    if ($user) {
                        $ga = new $this->Google_accounts_model(NULL, 'account');
                        $ga->setGoogleId($user->id,true);
                        $ga->setName($user->name);
                        $ga->setEmail($user->email);
                        $ga->setUserId($this->session->userId);
                        if( !$ga->nonEmpty() ) {
                            $ga->insert();
                        }
                    }
                }
                redirect( account_url("my-account-google") );
            break;
        }
    }

}
