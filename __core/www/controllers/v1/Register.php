<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller {
	
	var $list_limit = 12;

	public function __construct() {
		parent::__construct();
	}

	public function index($start=0) {	
		$this->load->view('register/index', $this->template_data->get_data());
	}

}