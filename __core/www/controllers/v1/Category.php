<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends PLACES_Controller {
	
	var $list_limit = 12;

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		$this->template_data->set('page_title', 'Categories');

		$this->load->view('places/categories', $this->template_data->get_data());
	}

	public function category($icon, $start=0) {
		
		$page_title = "Best " . place_icon_name($icon,true);
		if( $start ) {
			$page_num = ceil($start / $this->list_limit) + 1;;
			$page_title .= " - Page " . $page_num;
		}
		$this->template_data->set('page_title', $page_title);

		$places = new $this->Places_data_model('c', 'places');
		$places->cache_on();
		$places->set_order('c.lastmod', 'DESC');
		$places->setIcon($icon, true);
		$places->set_start( $start );
		$places->set_limit( $this->list_limit );
		$places->set_select('c.*');
		$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
		$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
		$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

		$this->template_data->set('places', $places->populate());
		$this->template_data->set('places_all', $places->count_all_results());

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'uri_segment' => 2,
			'base_url' => base_url( $this->config->item('index_page') . "/" . $icon),
			'total_rows' => $places->count_all_results(),
			'per_page' => $places->get_limit()
		)));

		$this->load->view('places/places_browse', $this->template_data->get_data());
	}

	public function states($category, $slug, $start=0) {

		$this->template_data->set('category', $category);

		$states = new $this->States_model('s', 'common');
		$states->setSlug($slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$this->template_data->set('state', $state);
			
					$page_title = place_icon_name($category,true) . " in " . $state->name;
					if( $start ) {
						$page_num = ceil($start / $this->list_limit) + 1;
						$page_title .= " - Page " . $page_num;
					}
					$this->template_data->set('page_title', $page_title);
			
			$places = new $this->Places_data_model('c', 'places');
			$places->cache_on();
			$places->set_order('c.lastmod', 'DESC');
			$places->set_start( $start );
			$places->set_limit( $this->list_limit );
			$places->set_select('c.*');
			$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
			$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
			$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');
			
				$cities_db = new $this->Cities_model('s', 'common');
				$cities_db->setStateId($state->id,true);
				$cities_db->set_select('s.id');
				$cities_db->set_limit(1);
				$cities_db->set_where('s.id=c.city_id');

			$places->set_where('('.$cities_db->get_compiled_select().') IS NOT NULL');
			$places->setIcon($category,true);

			$this->template_data->set('places', $places->populate());
			$this->template_data->set('places_all', $places->count_all_results());

			$this->template_data->set('pagination', bootstrap_pagination(array(
				'uri_segment' => 4,
				'base_url' => base_url( $this->config->item('index_page') . "/{$category}/in/{$slug}"),
				'total_rows' => $places->count_all_results(),
				'per_page' => $places->get_limit()
			)));

			$this->load->view('places/categories_states', $this->template_data->get_data());

		} else {
			$this->page_not_found();
		}
	}

	public function counties($category, $state_slug, $county_slug, $start=0) {

		$this->template_data->set('category', $category);
		
		$states = new $this->States_model('s', 'common');
		$states->setSlug($state_slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$this->template_data->set('state', $state);

			$counties = new $this->Counties_model('s', 'common');
			$counties->setSlug($county_slug,true);

			if( $counties->nonEmpty() ) {

				$county = $counties->get_results();
				$this->template_data->set('county', $county);

				/*
				$cities = new $this->Cities_model('s', 'common');
				$cities->setCountyId($county->id,true);
				$cities->set_limit(20);
				$cities->set_start($start);
				$this->template_data->set('cities', $cities->populate());	

				$this->template_data->set('pagination', bootstrap_pagination(array(
					'uri_segment' => 4,
					'base_url' => base_url( $this->config->item('index_page') . "in/{$state->slug}/{$county->slug}" ),
					'total_rows' => $cities->count_all_results(),
					'per_page' => $cities->get_limit()
				)));
				*/

					$page_title = place_icon_name($category,true) . " in " . $county->name;
					if( $start ) {
						$page_num = ceil($start / $this->list_limit) + 1;;
						$page_title .= " - Page " . $page_num;
					}
					$this->template_data->set('page_title', $page_title);
				
				$places = new $this->Places_data_model('c', 'places');
				$places->cache_on();
				$places->set_order('c.lastmod', 'DESC');
				$places->set_start( $start );
				$places->set_limit( $this->list_limit );
				$places->set_select('c.*');
				$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
				$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
				$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');

					$cities_db = new $this->Cities_model('s', 'common');
					$cities_db->setCountyId($county->id,true);
					$cities_db->set_select('s.id');
					$cities_db->set_limit(1);
					$cities_db->set_where('s.id=c.city_id');

				$places->set_where('('.$cities_db->get_compiled_select().') IS NOT NULL');
				$places->setIcon($category,true);

				$this->template_data->set('places', $places->populate());
				$this->template_data->set('places_all', $places->count_all_results());

				$this->template_data->set('pagination', bootstrap_pagination(array(
					'uri_segment' => 5,
					'base_url' => base_url( $this->config->item('index_page') . "/{$category}/in/{$state_slug}/{$county_slug}"),
					'total_rows' => $places->count_all_results(),
					'per_page' => $places->get_limit()
				)));

				$this->load->view('places/categories_counties', $this->template_data->get_data());

			} else {
				$this->page_not_found();
			}

		} else {
			$this->page_not_found();
		}
	}

	public function cities($category, $state_slug, $county_slug, $city_slug, $start=0) {

		$this->template_data->set('category', $category);
		
		$states = new $this->States_model('s', 'common');
		$states->setSlug($state_slug,true);
		
		if( $states->nonEmpty() ) {
			$state = $states->get_results();
			$this->template_data->set('state', $state);

			$counties = new $this->Counties_model('s', 'common');
			$counties->setSlug($county_slug,true);

			if( $counties->nonEmpty() ) {
				$county = $counties->get_results();
				$this->template_data->set('county', $county);

				$cities = new $this->Cities_model('s', 'common');
				$cities->setSlug($city_slug,true);

				if( $cities->nonEmpty() ) {
					$city = $cities->get_results();
					$this->template_data->set('city', $city);

					$page_title = place_icon_name($category,true) . " in " . $city->name;
					if( $start ) {
						$page_num = ceil($start / $this->list_limit) + 1;;
						$page_title .= " - Page " . $page_num;
					}
					$this->template_data->set('page_title', $page_title);

					$places = new $this->Places_data_model('c', 'places');
					$places->cache_on();
					$places->set_order('c.lastmod', 'DESC');
					$places->set_start( $start );
					$places->set_limit( $this->list_limit );
					$places->set_select('c.*');
					$places->set_select('(SELECT pp.photo_reference FROM places_photos pp WHERE pp.place_id=c.place_id ORDER BY RAND() LIMIT 1) as photo_reference');
					$places->set_select('(SELECT COUNT(*) FROM places_photos pp WHERE pp.place_id=c.place_id LIMIT 1) as photos_count');
					$places->set_select('(SELECT COUNT(*) FROM places_reviews pp WHERE pp.place_id=c.place_id LIMIT 1) as reviews_count');
					$places->setCityId($city->id,true);

					$this->template_data->set('places', $places->populate());
					$this->template_data->set('places_all', $places->count_all_results());

					$this->template_data->set('pagination', bootstrap_pagination(array(
						'uri_segment' => 6,
						'base_url' => base_url( $this->config->item('index_page') . "/{$category}/in/{$state->slug}/{$county->slug}/{$city->slug}" ),
						'total_rows' => $places->count_all_results(),
						'per_page' => $places->get_limit()
					)));

					$this->load->view('places/categories_cities', $this->template_data->get_data());
				} else {
					$this->page_not_found();
				}
				
			} else {
				$this->page_not_found();
			}

		} else {
			$this->page_not_found();
		}
		
	}

}
