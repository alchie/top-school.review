<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Template_data {
    
    var $CI;
    var $defaults;

    private $data = array(
         'page_title' => APP_DEFAULT_TITLE,
         'meta_description' => APP_DEFAULT_TITLE,
         'meta_keywords' => APP_DEFAULT_TITLE,
         'canonical_url' => false,
        );

    public function __construct()
    {
       $this->CI =& get_instance();
       $this->defaults = array(
            'og:title'=> $this->get('page_title'),
            'og:type'=>'website',
            'og:url'=> site_url( uri_string() ),
            'og:image'=>'',
            'canonical_url' => site_url( uri_string() ),
        );
    }
            
    function set($key, $value=NULL)
    {
        if( is_array($key) ) {
            foreach($key as $k=>$v) {
                $this->data[$k] = $v;
            }
        } else {
            $this->data[$key] = $value;
        }
    }
    
    function get($key)
    {
        return (isset($this->data[$key])) ? $this->data[$key] : null;
    }
    
    function get_data() {
        return $this->data;
    }

    function prepend($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $value . $current_value );  
    }
    
    function append($key, $value)
    {
        $current_value = $this->get($key);
        $this->set($key, $current_value . $value );        
    }
            
    function opengraph( $var=array() ) {        
        $opengraph = array_merge( (array) $this->defaults, (array) $var );
        $meta = '';
        foreach( $opengraph as $tag=>$value ) {
            if( $value != '' ) {
                $meta .= '<meta property="'.$tag.'" content="'.$value.'" />' . "\n";            
            }
        }
        $this->set('opengraph', $meta );
    }
    
}

/* End of file Global_variables.php */