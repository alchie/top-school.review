<?php defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH . 'sdk' . DIRECTORY_SEPARATOR . 'google-api-php-client' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php' );

class Google
{
    private $client;

    public function __construct()
    {
        // load config
        $this->load->config('google');

        // Load required libraries and helpers
        $this->load->library('session');
        $this->load->helper('url');

        $this->client = new Google_Client();
        $this->client->setApplicationName($this->config->item('google_app_name'));
        $this->client->setClientId($this->config->item('google_client_id'));
        $this->client->setClientSecret($this->config->item('google_client_secret'));
        $this->client->addScope('profile');
        $this->client->addScope('email');
        //$this->client->useApplicationDefaultCredentials();
        //$this->client->setAuthConfig( APPPATH . 'config' . DIRECTORY_SEPARATOR . 'google_client_secret.json' );
        //$this->client->addScope(Google_Service_Plus::PLUS_LOGIN);
        //$this->client->addScope(Google_Service_Plus::PLUS_ME);
        //$this->client->addScope(Google_Service_Plus::USERINFO_EMAIL);
        //$this->client->addScope(Google_Service_Plus::USERINFO_PROFILE);
        $uri = base_url( $this->config->item('google_redirect_uri') ); 
        $this->client->setRedirectUri( $uri );
    }

    public function login_url() {
        $uri = base_url( $this->config->item('google_redirect_uri') ); 
        $this->client->setRedirectUri( $uri );
        return $this->client->createAuthUrl();
    }

    public function connect_url() {
        $this->client->setRedirectUri( base_url( $this->config->item('google_connect_uri') ) );
        return $this->client->createAuthUrl();
    }

    public function is_authenticated()
    {
        $access_token = $this->authenticate();

        if (isset($access_token))
        {
            return $access_token;
        }

        return false;
    }

    private function authenticate()
    {
        $access_token = $this->get_access_token();

        if ($access_token)
        {
            $this->client->setAccessToken($access_token);
            return $access_token;
        }

        if (!$access_token)
        {
            try
            {
                $this->client->authenticate($this->input->get('code'));
                $access_token = $this->client->getAccessToken();
                //$access_token = $this->client->fetchAccessTokenWithAuthCode($this->input->get('code'));
                //var_dump( $access_token ); exit;
            }
            catch (Exception $e)
            {
                return null;
            }

            // If we got a session we need to exchange it for a long lived session.
            if (isset($access_token))
            {
                $this->set_access_token($access_token);
                $this->client->setAccessToken($access_token);
            }
        }

        return $access_token;
    }

    private function get_access_token()
    {
        return $this->session->userdata('google_access_token');
    }

    private function set_access_token($access_token)
    {
        $this->session->set_userdata('google_access_token', $access_token);
    }

    public function getUser() {
        $oauth2 = new Google_Service_Oauth2($this->client);
        $user = $oauth2->userinfo->get();
        return $user;
    }

    public function __get($var)
    {
        return get_instance()->$var;
    }


}
