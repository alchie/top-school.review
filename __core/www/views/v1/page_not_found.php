<?php $this->load->view('header'); ?>

	<section class="blog-area section">
		<div class="container">

			<div class="row">
				<div class="col-lg-2 col-md-0"></div>
				<div class="col-lg-8 col-md-12">
					<div class="post-wrapper">

						<h3 class="title"><b>ERROR 404!</b></h3>
						<p>Page Not Found</p>
					


					</div><!-- post-wrapper -->
				</div><!-- col-sm-8 col-sm-offset-2 -->
			</div><!-- row -->

		</div><!-- container -->
	</section><!-- section -->

<?php $this->load->view('footer'); ?>