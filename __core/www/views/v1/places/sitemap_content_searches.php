<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($search_items as $item) { 
	echo '<url><loc>'.site_url("search_{$item->slug}").'</loc></url>';
}
echo '</urlset>';
