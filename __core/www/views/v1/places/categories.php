<?php $this->load->view('header'); ?>

  <section class="blog-area section">
    <div class="container">

      <div class="row">
     	 <div class="col-md-12">

		<div class="panel panel-default">
		  <div class="panel-heading">
		    <h3 class="panel-title">Categories</h3>
		  </div>
<div class="list-group">
<?php foreach(array(
	'restaurant'=>'Restaurant',
	'bar'=>'Bar',
	'cafe'=>'Cafe',
	'shopping'=>'Shopping',
	'gas_station'=>'Gas Station',
	'lodging'=>'Hotels',
	'school'=>'Schools',
	'generic_business'=>'General Businesses',
) as $uri=>$name) { ?>
  <a class="list-group-item" href="<?php echo site_url($uri); ?>"><?php echo $name; ?></a>
<?php } ?>
</div>
		</div>

	      </div>
	  </div>
	</div>
	</section>

<?php $this->load->view('footer'); ?>