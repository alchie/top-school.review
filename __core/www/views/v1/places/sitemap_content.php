<?php 
echo '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($places as $place) { 
	echo '<url><loc>'.site_url("{$place->slug}").'</loc></url>';
	echo '<url><loc>'.site_url("amp_{$place->slug}").'</loc></url>';
}
echo '</urlset>';
