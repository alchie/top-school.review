
                    <div class="single-post info-area">

                        <div class="sidebar-area about-area">
                            <h4 class="title"><i class="ion-map"></i> <b>Address</b></h4>
                            <p><a href="<?php echo site_url("{$current_place->slug}"); ?>"><?php echo $current_place->vicinity; ?></a></p>
                        </div>

<?php if( $current_place->formatted_phone_number ) { ?>
                        <div class="sidebar-area about-area">
                            <h4 class="title"><i class="ion-android-call"></i> <b>Telephone Number</b></h4>
                            <p><?php echo $current_place->formatted_phone_number; ?></p>
                        </div>
<?php } ?>
<?php if( $current_place->website ) { ?>
                        <div class="sidebar-area about-area">
                            <h4 class="title"><i class="ion-link"></i> <b>Website</b></h4>
                            <p><a href="<?php echo $current_place->website; ?>" target="_blank"><?php echo $current_place->website; ?></a></p>
                        </div>
<?php } ?>
<!--
                        <div class="sidebar-area subscribe-area">

                            <h4 class="title"><b>SUBSCRIBE</b></h4>
                            <div class="input-area">
                                <form>
                                    <input class="email-input" type="text" placeholder="Enter your email">
                                    <button class="submit-btn" type="submit"><i class="icon ion-ios-email-outline"></i></button>
                                </form>
                            </div>

                        </div>
-->
                        <!-- subscribe-area -->



                    </div><!-- info-area -->

