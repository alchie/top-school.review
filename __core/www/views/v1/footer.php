 <footer>

    <div class="container">
      <div class="row">

        <div class="col-lg-4 col-md-6">
          <div class="footer-section">

            <a class="logo" href="#"><img src="<?php echo base_url("assets/images/logo.png"); ?>" alt="Logo Image"></a>
            <p class="copyright">Designed by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
            <ul class="icons">
              <li><a href="#"><i class="ion-social-facebook-outline"></i></a></li>
              <li><a href="#"><i class="ion-social-twitter-outline"></i></a></li>
              <li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
              <li><a href="#"><i class="ion-social-vimeo-outline"></i></a></li>
              <li><a href="#"><i class="ion-social-pinterest-outline"></i></a></li>
            </ul>

          </div><!-- footer-section -->
        </div><!-- col-lg-4 col-md-6 -->

        <div class="col-lg-4 col-md-6">
            <div class="footer-section">
            <h4 class="title"><b>CATAGORIES</b></h4>
            <ul>
              <li><a href="<?php echo site_url("restaurant"); ?>">RESTAURANT</a></li>
              <li><a href="<?php echo site_url("bar"); ?>">BAR</a></li>
              <li><a href="<?php echo site_url("cafe"); ?>">CAFE</a></li>
            </ul>
            <ul>
              <li><a href="<?php echo site_url("shopping"); ?>">SHOPPING</a></li>
              <li><a href="<?php echo site_url("gas_station"); ?>">GAS STATION</a></li>
            </ul>
          </div><!-- footer-section -->
        </div><!-- col-lg-4 col-md-6 -->

        <div class="col-lg-4 col-md-6">
          <div class="footer-section">
<!--
            <h4 class="title"><b>SUBSCRIBE</b></h4>
            <div class="input-area">
              <form>
                <input class="email-input" type="text" placeholder="Enter your email">
                <button class="submit-btn" type="submit"><i class="icon ion-ios-email-outline"></i></button>
              </form>
            </div>
-->
          </div><!-- footer-section -->
        </div><!-- col-lg-4 col-md-6 -->

      </div><!-- row -->
    </div><!-- container -->
  </footer>


  <!-- SCIPTS -->

  <script src="<?php echo base_url("assets/js/jquery-3.1.1.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/js/tether.min.js"); ?>"></script>
  <script src="<?php echo base_url("assets/js/bootstrap.js"); ?>"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/vanilla-lazyload/8.10.0/lazyload.min.js"></script>
  <script src="<?php echo base_url("assets/js/scripts.js"); ?>"></script>
  <script>
  	<!--
		new LazyLoad();
  	//-->
  </script>
</body>
</html>