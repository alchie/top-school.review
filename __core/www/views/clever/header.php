<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title><?php echo $page_title; ?></title>
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="keywords" content="<?php echo $meta_keywords; ?>">
  <?php echo (isset($opengraph)) ? $opengraph : ''; ?>
  <link rel="alternate" href="<?php echo base_url(); ?>" hreflang="x-default" />

    <!-- Stylesheet -->
  <link href="<?php echo base_url("assets/style.css"); ?>" rel="stylesheet">

<?php 
echo (isset($canonical_url)) ? '<link rel="canonical" href="'.$canonical_url.'" />'."\n" : ''; 
echo (isset($amphtml_url)) ? '<link rel="amphtml" href="'.$amphtml_url.'" />'."\n" : '';  
?>

</head>

<body>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- Top Header Area -->
        <div class="top-header-area d-flex justify-content-between align-items-center">
            <!-- Contact Info -->
            <div class="contact-info">
                
                <a href="#"><span>Email:</span> info@top-school.review</a>
            </div>
            <!-- Follow Us -->
            <div class="follow-us">
                <span>Follow us</span>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="clever-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <!-- Menu -->
                <nav class="classy-navbar justify-content-between" id="cleverNav">

                    <!-- Logo -->
                    <a class="nav-brand" href="<?php echo site_url(); ?>"><img src="<?php echo base_url("assets/img/core-img/logo.png"); ?>" alt=""></a>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <?php $this->load->view('navbar'); ?>

                </nav>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->