<?php $this->load->view('header'); ?>

 <!-- ##### Popular Course Area Start ##### -->
    <section class="popular-courses-area section-padding-100">
        <div class="container">
            <div class="row">

<?php

if( $places ) {

$n = 0;
foreach($places as $place) { ?>
                <!-- Single Popular Course -->
                <div class="col-12 col-md-6 col-lg-4">
                  <?php $this->load->view('schools_directory/card', array('place'=>$place)); ?>
                </div>
<?php
$n++;
  if( $n == 3) {
    $n = 0;
    echo '</div><div class="row"><!-- split -->';
  }
} 
?>

<?php } else { ?>

<?php } ?>

            </div>



<?php if( $pagination ) { ?>

            <div class="row">
                <div class="col-12">
                    <div class="load-more text-center wow fadeInUp" data-wow-delay="1000ms">
                        <div class="btn clever-btn btn-2">
                          <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
<?php } ?>

        </div>
    </section>
    <!-- ##### Popular Course Area End ##### -->


<?php $this->load->view('footer'); ?>