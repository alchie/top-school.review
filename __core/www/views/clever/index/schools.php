   <!-- ##### Popular Courses Start ##### -->
    <section class="popular-courses-area section-padding-100-0" style="background-image: url(img/core-img/texture.png);">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section-heading">
                        <h3>Random Schools</h3>
                    </div>
                </div>
            </div>


            <div class="row">

<?php foreach( $places as $place ) { ?>
                <!-- Single Popular Course -->
                <div class="col-12 col-md-6 col-lg-4">
                    <?php $this->load->view( 'index/card', array('place'=>$place) ); ?>
                </div>
<?php } ?>


            </div>
        </div>
    </section>
    <!-- ##### Popular Courses End ##### -->