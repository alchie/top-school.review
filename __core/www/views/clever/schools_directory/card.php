
                    <div class="single-popular-course mb-100 wow fadeInUp" data-wow-delay="250ms">
                        <img src="<?php echo base_url("assets/img/bg-img/c1.jpg"); ?>" alt="">
                        <!-- Course Content -->
                        <div class="course-content">
                            <h4><?php echo $place->name; ?></h4>
                            <p><?php echo $place->vicinity; ?></p>
                        </div>
                        <!-- Seat Rating Fee -->
                        <div class="seat-rating-fee d-flex justify-content-between">
                            <div class="seat-rating h-100 d-flex align-items-center">
                                <div class="seat">
                                    <i class="fa fa-user" aria-hidden="true"></i> <?php echo $place->reviews_count; ?>
                                </div>
                                <div class="rating">
                                    <i class="fa fa-star" aria-hidden="true"></i> <?php echo number_format($place->rating,1); ?>
                                </div>
                            </div>
                            <div class="course-fee h-100">
                                <a href="<?php echo site_url($place->slug); ?>" class="free">View</a>
                            </div>
                        </div>
                    </div>
                