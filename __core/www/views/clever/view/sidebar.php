                <div class="col-12 col-lg-4">
                    <div class="course-sidebar">


                        <!-- Widget -->
                        <div class="sidebar-widget">
                            <h4>Other Schools</h4>
<?php foreach( $other_places as $oplace ) { ?>
                            <!-- Single Courses -->
                            <a class="single--courses d-flex align-items-center" href="<?php echo site_url($oplace->slug); ?>">
                                <div class="thumb">
                                    <img src="<?php echo base_url("assets/img/bg-img/yml.jpg"); ?>" alt="">
                                </div>
                                <div class="content">
                                    <h5><?php echo $oplace->name; ?></h5>
                                    <h6><?php echo $oplace->vicinity; ?></h6>
                                </div>
                            </a>
<?php } ?>

                        </div>
                    </div>
                </div>