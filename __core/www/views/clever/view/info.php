<?php $this->load->view('header'); ?>


    <!-- ##### Breadcumb Area Start ##### -->
    <div class="breadcumb-area">
        <!-- Breadcumb -->
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo site_url(); ?>">Home</a></li>
                <li class="breadcrumb-item"><a href="<?php echo site_url("directory"); ?>">Directory</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $current_place->name; ?></li>
            </ol>
        </nav>
    </div>
    <!-- ##### Breadcumb Area End ##### -->

    <!-- ##### Single Course Intro Start ##### -->
    <div class="single-course-intro d-flex align-items-center justify-content-center" style="background-image: url(<?php echo base_url("assets/img/bg-img/bg3.jpg"); ?>)">
        <!-- Content -->
        <div class="single-course-intro-content text-center">
            <!-- Ratings -->
            <div class="ratings">
<?php for($i=1;$i<=5; $i++) { ?>
                <i class="fa fa-star<?php echo ($i >= $current_place->rating) ? '-o' : ''; ?>" aria-hidden="true"></i>
<?php } ?>
            </div>
            <h3><?php echo $current_place->name; ?></h3>
            <div class="meta d-flex align-items-center justify-content-center">
                    <?php echo $current_place->vicinity; ?>
            </div>
<?php if( $current_place->formatted_phone_number ) { ?>
            <div class="price"><?php echo $current_place->formatted_phone_number; ?></div>
<?php } ?>
        </div>
    </div>
    <!-- ##### Single Course Intro End ##### -->

    <!-- ##### Courses Content Start ##### -->
    <div class="single-course-content section-padding-100">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8">
                    <div class="course--content">

                        <div class="clever-tabs-content">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="tab--1" data-toggle="tab" href="#tab1" role="tab" aria-controls="tab1" aria-selected="false">Description</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--2" data-toggle="tab" href="#tab2" role="tab" aria-controls="tab2" aria-selected="true">Courses</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--3" data-toggle="tab" href="#tab3" role="tab" aria-controls="tab3" aria-selected="true">Reviews</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--4" data-toggle="tab" href="#tab4" role="tab" aria-controls="tab4" aria-selected="true">Students</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="tab--5" data-toggle="tab" href="#tab5" role="tab" aria-controls="tab5" aria-selected="true">Teachers</a>
                                </li>
                            </ul>

                            <div class="tab-content" id="myTabContent">


<?php $this->load->view('view/tabs'); ?>

                            </div>
                        </div>
                    </div>
                </div>

<?php $this->load->view('view/sidebar'); ?>

            </div>
        </div>
    </div>
    <!-- ##### Courses Content End ##### -->

 
<?php $this->load->view('footer'); ?>