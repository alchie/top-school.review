<?php $this->load->view('header'); ?>


    <!-- ##### Regular Page Area Start ##### -->
    <div class="regular-page-area section-padding-100">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-4">
                    <div class="page-content">
<a class="social-btn btn btn-block btn-social btn-google" href="<?php echo site_url("login/api/google"); ?>">
    <span class="fa fa-google"></span> Sign in with Google
</a>
<a class="social-btn btn btn-block btn-social btn-facebook" href="<?php echo site_url("login/api/facebook"); ?>">
    <span class="fa fa-facebook"></span> Sign in with Facebook
</a>
<a class="social-btn btn btn-block btn-social btn-twitter" href="<?php echo site_url("login/api/twitter"); ?>">
    <span class="fa fa-twitter"></span> Sign in with Twitter
</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Regular Page Area End ##### -->

<?php $this->load->view('footer'); ?>