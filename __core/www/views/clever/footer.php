    <!-- ##### Footer Area Start ##### -->
    <footer class="footer-area">
        <!-- Top Footer Area -->
        <div class="top-footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!-- Footer Logo -->
                        <div class="footer-logo">
                            <a href="<?php echo site_url(); ?>"><img src="<?php echo base_url("assets/img/core-img/logo2.png"); ?>" alt=""></a>
                        </div>
                        <!-- Copywrite -->
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bottom Footer Area -->
        <div class="bottom-footer-area d-flex justify-content-between align-items-center">
            <!-- Contact Info -->
            <div class="contact-info">
                <a href="#"><span>Email:</span> info@top-school.review</a>
            </div>
            <!-- Follow Us -->
            <div class="follow-us">
                <span>Follow us</span>
                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <script src="<?php echo base_url("assets/js/jquery/jquery-2.2.4.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap/popper.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/bootstrap/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/plugins/plugins.js"); ?>"></script>
    <script src="<?php echo base_url("assets/js/active.js"); ?>"></script>
</body>

</html>