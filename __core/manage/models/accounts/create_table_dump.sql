CREATE TABLE `accounts_facebook` (
  `user_id` int(20) DEFAULT NULL,
  `facebook_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`facebook_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_id` (`user_id`)
);

-- Table structure for table `accounts_facebook` 

CREATE TABLE `accounts_google` (
  `user_id` int(20) DEFAULT NULL,
  `google_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`google_id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_id` (`user_id`)
);

-- Table structure for table `accounts_google` 

CREATE TABLE `accounts_recovery` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL,
  `code` varchar(200) NOT NULL,
  `expiry` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

-- Table structure for table `accounts_recovery` 

CREATE TABLE `accounts_users` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL COMMENT 'add_email',
  `full_name` varchar(50) NOT NULL COMMENT 'add_inputbox',
  `password` text NOT NULL COMMENT 'add_password',
  `type` varchar(20) NOT NULL DEFAULT 'user' COMMENT 'add_dropdown',
  `verified` int(1) NOT NULL DEFAULT '0' COMMENT 'add_checkbox',
  `code` text,
  `date_registered` datetime DEFAULT CURRENT_TIMESTAMP,
  `referred_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
);

-- Table structure for table `accounts_users` 

