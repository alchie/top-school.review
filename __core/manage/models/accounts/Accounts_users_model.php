<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounts_users_model Class
 *
 * Manipulates `accounts_users` table on database

CREATE TABLE `accounts_users` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL COMMENT 'add_email',
  `full_name` varchar(50) NOT NULL COMMENT 'add_inputbox',
  `password` text NOT NULL COMMENT 'add_password',
  `type` varchar(20) NOT NULL DEFAULT 'user' COMMENT 'add_dropdown',
  `verified` int(1) NOT NULL DEFAULT '0' COMMENT 'add_checkbox',
  `code` text,
  `date_registered` datetime DEFAULT CURRENT_TIMESTAMP,
  `referred_by` int(20) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `email` (`email`)
);

ALTER TABLE  `accounts_users` ADD  `uid` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE  `accounts_users` ADD  `email` varchar(100) NOT NULL   UNIQUE KEY;
ALTER TABLE  `accounts_users` ADD  `full_name` varchar(50) NOT NULL   ;
ALTER TABLE  `accounts_users` ADD  `password` text NOT NULL   ;
ALTER TABLE  `accounts_users` ADD  `type` varchar(20) NOT NULL   DEFAULT 'user';
ALTER TABLE  `accounts_users` ADD  `verified` int(1) NOT NULL   DEFAULT '0';
ALTER TABLE  `accounts_users` ADD  `code` text NULL   ;
ALTER TABLE  `accounts_users` ADD  `date_registered` datetime NULL   DEFAULT 'CURRENT_TIMESTAMP';
ALTER TABLE  `accounts_users` ADD  `referred_by` int(20) NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.5.0
 */
 
class Accounts_users_model extends MY_Model {

	protected $uid;
	protected $email;
	protected $full_name;
	protected $password;
	protected $type;
	protected $verified;
	protected $code;
	protected $date_registered;
	protected $referred_by;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'accounts_users';
		$this->_short_name = 'accounts_users';
		$this->_fields = array("uid","email","full_name","password","type","verified","code","date_registered","referred_by");
		$this->_required = array("email","full_name","password","type","verified");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: uid -------------------------------------- 

	/** 
	* Sets a value to `uid` variable
	* @access public
	*/

	public function setUid($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('uid', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `uid` variable
	* @access public
	*/

	public function getUid() {
		return $this->uid;
	}
	
// ------------------------------ End Field: uid --------------------------------------


// ---------------------------- Start Field: email -------------------------------------- 

	/** 
	* Sets a value to `email` variable
	* @access public
	*/

	public function setEmail($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('email', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `email` variable
	* @access public
	*/

	public function getEmail() {
		return $this->email;
	}
	
// ------------------------------ End Field: email --------------------------------------


// ---------------------------- Start Field: full_name -------------------------------------- 

	/** 
	* Sets a value to `full_name` variable
	* @access public
	*/

	public function setFullName($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('full_name', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `full_name` variable
	* @access public
	*/

	public function getFullName() {
		return $this->full_name;
	}
	
// ------------------------------ End Field: full_name --------------------------------------


// ---------------------------- Start Field: password -------------------------------------- 

	/** 
	* Sets a value to `password` variable
	* @access public
	*/

	public function setPassword($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('password', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `password` variable
	* @access public
	*/

	public function getPassword() {
		return $this->password;
	}
	
// ------------------------------ End Field: password --------------------------------------


// ---------------------------- Start Field: type -------------------------------------- 

	/** 
	* Sets a value to `type` variable
	* @access public
	*/

	public function setType($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('type', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `type` variable
	* @access public
	*/

	public function getType() {
		return $this->type;
	}
	
// ------------------------------ End Field: type --------------------------------------


// ---------------------------- Start Field: verified -------------------------------------- 

	/** 
	* Sets a value to `verified` variable
	* @access public
	*/

	public function setVerified($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('verified', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `verified` variable
	* @access public
	*/

	public function getVerified() {
		return $this->verified;
	}
	
// ------------------------------ End Field: verified --------------------------------------


// ---------------------------- Start Field: code -------------------------------------- 

	/** 
	* Sets a value to `code` variable
	* @access public
	*/

	public function setCode($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('code', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `code` variable
	* @access public
	*/

	public function getCode() {
		return $this->code;
	}
	
// ------------------------------ End Field: code --------------------------------------


// ---------------------------- Start Field: date_registered -------------------------------------- 

	/** 
	* Sets a value to `date_registered` variable
	* @access public
	*/

	public function setDateRegistered($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('date_registered', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `date_registered` variable
	* @access public
	*/

	public function getDateRegistered() {
		return $this->date_registered;
	}
	
// ------------------------------ End Field: date_registered --------------------------------------


// ---------------------------- Start Field: referred_by -------------------------------------- 

	/** 
	* Sets a value to `referred_by` variable
	* @access public
	*/

	public function setReferredBy($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('referred_by', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `referred_by` variable
	* @access public
	*/

	public function getReferredBy() {
		return $this->referred_by;
	}
	
// ------------------------------ End Field: referred_by --------------------------------------



	
	public function get_table_options() {
		return array(
			'uid' => (object) array(
										'Field'=>'uid',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>'auto_increment'
									),

			'email' => (object) array(
										'Field'=>'email',
										'Type'=>'varchar(100)',
										'Null'=>'NO',
										'Key'=>'UNI',
										'Default'=>'',
										'Extra'=>''
									),

			'full_name' => (object) array(
										'Field'=>'full_name',
										'Type'=>'varchar(50)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'password' => (object) array(
										'Field'=>'password',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'type' => (object) array(
										'Field'=>'type',
										'Type'=>'varchar(20)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'user',
										'Extra'=>''
									),

			'verified' => (object) array(
										'Field'=>'verified',
										'Type'=>'int(1)',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'0',
										'Extra'=>''
									),

			'code' => (object) array(
										'Field'=>'code',
										'Type'=>'text',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									),

			'date_registered' => (object) array(
										'Field'=>'date_registered',
										'Type'=>'datetime',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'CURRENT_TIMESTAMP',
										'Extra'=>''
									),

			'referred_by' => (object) array(
										'Field'=>'referred_by',
										'Type'=>'int(20)',
										'Null'=>'YES',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'uid' => "ALTER TABLE  `accounts_users` ADD  `uid` int(20) NOT NULL  AUTO_INCREMENT PRIMARY KEY;",
			'email' => "ALTER TABLE  `accounts_users` ADD  `email` varchar(100) NOT NULL   UNIQUE KEY;",
			'full_name' => "ALTER TABLE  `accounts_users` ADD  `full_name` varchar(50) NOT NULL   ;",
			'password' => "ALTER TABLE  `accounts_users` ADD  `password` text NOT NULL   ;",
			'type' => "ALTER TABLE  `accounts_users` ADD  `type` varchar(20) NOT NULL   DEFAULT 'user';",
			'verified' => "ALTER TABLE  `accounts_users` ADD  `verified` int(1) NOT NULL   DEFAULT '0';",
			'code' => "ALTER TABLE  `accounts_users` ADD  `code` text NULL   ;",
			'date_registered' => "ALTER TABLE  `accounts_users` ADD  `date_registered` datetime NULL   DEFAULT 'CURRENT_TIMESTAMP';",
			'referred_by' => "ALTER TABLE  `accounts_users` ADD  `referred_by` int(20) NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Accounts_users_model.php */
/* Location: ./application/models/Accounts_users_model.php */
