<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Places_redirect_model Class
 *
 * Manipulates `places_redirect` table on database

CREATE TABLE `places_redirect` (
  `place_id` int(20) NOT NULL,
  `url` text NOT NULL,
  PRIMARY KEY (`place_id`)
);

ALTER TABLE  `places_redirect` ADD  `place_id` int(20) NOT NULL   PRIMARY KEY;
ALTER TABLE  `places_redirect` ADD  `url` text NOT NULL   ;


 * @package			        Model
 * @version_number	        5.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Model Generator (CMG) v3.4.0
 */
 
class Places_redirect_model extends MY_Model {

	protected $place_id;
	protected $url;

	// --------------------------------------------------------------------

	/**
	* Construct 
	* @access public
	* @param  String
	* @return Boolean;
	*/

	function __construct($short_name=NULL, $db_config=NULL) {
		$this->_table_name = 'places_redirect';
		$this->_short_name = 'places_redirect';
		$this->_fields = array("place_id","url");
		$this->_required = array("url");
		parent::__construct($short_name, $db_config);
	}

	// --------------------------------------------------------------------


// ---------------------------- Start Field: place_id -------------------------------------- 

	/** 
	* Sets a value to `place_id` variable
	* @access public
	*/

	public function setPlaceId($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('place_id', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `place_id` variable
	* @access public
	*/

	public function getPlaceId() {
		return $this->place_id;
	}
	
// ------------------------------ End Field: place_id --------------------------------------


// ---------------------------- Start Field: url -------------------------------------- 

	/** 
	* Sets a value to `url` variable
	* @access public
	*/

	public function setUrl($value, $setWhere=FALSE, $set_data_field=FALSE, $whereOperator=NULL, $underCondition=NULL, $priority=NULL) {
		return $this->_set_field('url', $value, $setWhere, $set_data_field, $whereOperator, $underCondition, $priority);
	}
	
	/** 
	* Get the value of `url` variable
	* @access public
	*/

	public function getUrl() {
		return $this->url;
	}
	
// ------------------------------ End Field: url --------------------------------------



	
	public function get_table_options() {
		return array(
			'place_id' => (object) array(
										'Field'=>'place_id',
										'Type'=>'int(20)',
										'Null'=>'NO',
										'Key'=>'PRI',
										'Default'=>'',
										'Extra'=>''
									),

			'url' => (object) array(
										'Field'=>'url',
										'Type'=>'text',
										'Null'=>'NO',
										'Key'=>'',
										'Default'=>'',
										'Extra'=>''
									)
		);
	}

	public function add_table_column($field_name) {
		$column = array(
			'place_id' => "ALTER TABLE  `places_redirect` ADD  `place_id` int(20) NOT NULL   PRIMARY KEY;",
			'url' => "ALTER TABLE  `places_redirect` ADD  `url` text NOT NULL   ;",
		);

		if( isset( $column[$field_name] ) ) {
			$this->db->query( $column[$field_name] );
		}
	}

}

/* End of file Places_redirect_model.php */
/* Location: ./application/models/Places_redirect_model.php */
