<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('bootstrap_pagination'))
{
    function bootstrap_pagination($config=array(), $url_suffix=NULL)
    {
        $defaults['base_url'] = base_url();
        $defaults['total_rows'] = 10;
        $defaults['per_page'] = 0;
        $defaults['full_tag_open'] = '<div class="btn-group btn-group-sm">';
        $defaults['full_tag_close'] = '</div>';

        $defaults['cur_tag_open'] = '<div class="btn btn-default active"><strong>';
        $defaults['cur_tag_close'] = '</strong></div>';

        $defaults['attributes'] = array('class' => 'btn btn-default');

        $defaults['suffix'] = '.html';
        
        $new_config = array_merge($defaults, $config);

        if( isset($config['ajax']) && ($config['ajax']===true) ) {
            $new_config['attributes']['class'] .= ' ajaxPage';
        }

        $CI = get_instance();
        $pagination = new $CI->pagination;
        $pagination->initialize($new_config);

        $pagination_html = $pagination->create_links();

        //if( $CI->config->item('url_suffix') ) {
            //$pagination_html = str_replace('" class="', $CI->config->item('url_suffix') . '" class="', $pagination_html );
        //}

        if( $url_suffix ) {
            $pagination_html = str_replace('" class="', $url_suffix . '" class="', $pagination_html);
        }
        
        return $pagination_html;
    }
}

if ( ! function_exists('bootstrap_alert'))
{
    function bootstrap_alert($message, $type='success')
    {
        $html = '<div class="alert alert-'.$type.' alert-dismissible" role="alert">';
        $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $html .= $message.'</div>';
        return $html;
    }
}

if ( ! function_exists('bootstrap_response_code'))
{
    function bootstrap_response_code($code)
    {
        $display=false;
        switch ($code) {
            case '201':
                $type='success';
                $message='Added Successfully!';
                $display=true;
            break;
            case '202':
                $type='warning';
                $message='Updated Successfully!';
                $display=true;
            break;
            case '203':
                $type='danger';
                $message='Deleted Successfully!';
                $display=true;
            break;
        }
        $html = '';
        if( $display ) {
            $html .= '<div class="alert alert-'.$type.' alert-dismissible" role="alert">';
            $html .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
            $html .= $message.'</div>';
        }
        return $html;
    }
}
// ------------------------------------------------------------------------