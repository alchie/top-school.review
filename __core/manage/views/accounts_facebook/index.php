<?php
/**
 * @package						Accounts_facebook List File
 * @version_number		1.0.0
 * @project						Trokis Philippines
 * @project_link			http://www.trokis.com
 * @author						Chester Alan Tagudin
 * @author_link				http://www.chesteralan.com
 * @generator					CodeIgniter Generator (CG) v3.5.0
 */
?>
<?php $this->load->view('header'); ?>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
<?php $this->load->view('sidebar'); ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Accounts Facebook</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Accounts Facebook</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

<?php if( $this->input->get('resp_code') ) { echo bootstrap_response_code( $this->input->get('resp_code') ); } ?>

<div class="box">
            <div class="box-header">
              <h3 class="box-title">Accounts Facebook List</h3>

              <div class="box-tools">
<form method="get">
                <div class="input-group input-group-sm" style="width: 350px;">
                  <input type="text" name="q" class="form-control pull-right" placeholder="Search Accounts Facebook" value="<?php echo $this->input->get('q'); ?>">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    <a href="<?php echo site_url("{$page_name}/add"); ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add New</a>
                  </div>
                </div>
</form>
              </div>

            </div>

            <!-- /.box-header -->
            <div class="box-body no-padding">
<?php if( $data ) { ?>
              <table class="table table-hover">
                <tbody><tr>
                
                  <th style="width: 80px">Action</th>
                </tr>
<?php foreach($data as $item) { ?>
                <tr>
                  
                  <td>
<div class="btn-group">
    <a href="<?php echo site_url("{$page_name}/edit/{$item->facebook_id}"); ?>" class="btn btn-default btn-xs">Edit</a>
    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
      <span class="caret"></span>
      <span class="sr-only">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right" role="menu">
      <li><a class="btn-confirm" href="<?php echo site_url("{$page_name}/delete/{$item->facebook_id}"); ?>?next=<?php echo uri_string(); ?>">Delete</a></li>
    </ul>
</div>                    
                  </td>
                </tr>
<?php } ?>
              </tbody></table>

<div class="box-footer clearfix"><?php echo $pagination; ?></div>
            
<?php } else { ?>
  <p class="text-center" style="margin: 100px 0;">Nothing Found!</p>
<?php } ?>
            </div>
            <!-- /.box-body -->
          </div>



    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
