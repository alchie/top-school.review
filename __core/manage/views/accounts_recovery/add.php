<?php 
/**
 * @package			Accounts_recovery Add File
 * @version_number		1.0.0
 * @project			Trokis Philippines
 * @project_link		http://www.trokis.com
 * @author			Chester Alan Tagudin
 * @author_link			http://www.chesteralan.com
 * @generator			CodeIgniter Generator (CG) v3.5.0
 */
?>
<?php $this->load->view('header'); ?>
  <aside class="main-sidebar">
<?php $this->load->view('sidebar'); ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Accounts Recovery
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Accounts Recovery</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

<?php if( $this->input->get('resp_code') ) { echo bootstrap_response_code( $this->input->get('resp_code') ); } ?>

<div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Add Accounts Recovery</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post">
              <div class="box-body">
<!-- start_%(field_name)s --><!-- end_%(field_name)s -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo site_url($page_name); ?>" class="btn btn-warning">Back</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
