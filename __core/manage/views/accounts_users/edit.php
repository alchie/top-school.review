<?php 
/**
 * @package			Accounts_users Edit File
 * @version_number		1.0.0
 * @project			Trokis Philippines
 * @project_link		http://www.trokis.com
 * @author			Chester Alan Tagudin
 * @author_link			http://www.chesteralan.com
 * @generator			CodeIgniter Generator (CG) v3.5.0
 */
?>
<?php $this->load->view('header'); ?>
  <aside class="main-sidebar">
<?php $this->load->view('sidebar'); ?>
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Accounts Users
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Accounts Users</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

<?php if( $this->input->get('resp_code') ) { echo bootstrap_response_code( $this->input->get('resp_code') ); } ?>

<div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Accounts Users</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post">
              <div class="box-body">
<!-- start_%(field_name)s -->
<!-- start_email_email -->
<div class="form-group">
  <label for="email_email">Email</label>
  <input type="text" id="email_email" name="email" class="form-control" placeholder="Email" value="<?php echo $data->email; ?>">
</div>
<!-- end_email_email -->

<!-- start_inputbox_full_name -->
<div class="form-group">
  <label for="inputbox_full_name">Full Name</label>
  <input type="text" id="inputbox_full_name" name="full_name" class="form-control" placeholder="Full Name" value="<?php echo $data->full_name; ?>">
</div>
<!-- end_inputbox_full_name -->

<!-- start_password_password -->
<div class="form-group">
  <label for="password_password">Password</label>
  <input type="password" id="password_password" name="password" class="form-control" placeholder="Password">
</div>
<!-- end_password_password -->

<!-- start_dropdown_type -->
<div class="form-group">
        <label for="dropdown_type">Type</label>
        <select name="type" id="dropdown_type" class="form-control">
                <option value="">- - Select Type - -</option>
        </select>
</div>
<!-- end_dropdown_type -->
<!-- end_%(field_name)s -->
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="<?php echo site_url($page_name); ?>" class="btn btn-warning">Back</a>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php $this->load->view('footer'); ?>
