  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("assets/bower_components/jquery/dist/jquery.min.js"); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("assets/bower_components/bootstrap/dist/js/bootstrap.min.js"); ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/bower_components/fastclick/lib/fastclick.js"); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/dist/js/adminlte.min.js"); ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url("assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"); ?>"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url("assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"); ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"); ?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url("assets/bower_components/chart.js/Chart.js"); ?>"></script>
<script src="<?php echo base_url("assets/dist/js/script.js"); ?>"></script>

</body>
</html>
