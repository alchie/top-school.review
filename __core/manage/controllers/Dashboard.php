<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MAIN_Controller {
	
	private $ctrl_title = 'Dashboard';
	private $ctrl_name = 'dashboard';

	public function __construct() {
		parent::__construct();

		$this->template_data->set('page_title', $this->ctrl_title);
		$this->template_data->set('page_name', $this->ctrl_name);
		
	}

	public function index($start=0) {	
		$this->load->view($this->ctrl_name . '/index', $this->template_data->get_data());
	}

}
