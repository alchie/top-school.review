<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Places_categories extends MAIN_Controller {
	
	private $ctrl_title = 'Categories';
	private $ctrl_name = 'places_categories';

	public function __construct() {
		parent::__construct();

		$this->template_data->set('page_title', $this->ctrl_title);
		$this->template_data->set('page_name', $this->ctrl_name);
	}

	public function index($start=0) {	

		$categories = new $this->Categories_model('c', 'places');
		if( $this->input->get('q') ) {
			$categories->set_where('c.name LIKE "%'.$this->input->get('q').'%"');
		}
		$this->template_data->set( 'categories', $categories->populate() );

		$this->load->view($this->ctrl_name . '/index', $this->template_data->get_data());
	}

	public function add() {	

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('category_name', 'Category Name', 'required');
			if ( $this->form_validation->run() ) {
				$category = new $this->Categories_model('c', 'places');
				$category->setName( $this->input->post('category_name'), true );
				$category->setSlug( slugify( $this->input->post('category_name') ) );
				$category->setParentCategory( ($this->input->post('parent_category')) ? $this->input->post('parent_category') : 0 );
				$category->setPriority( $this->input->post('priority')  );
				$category->setActive( ($this->input->post('active')) ? 1 : 0 );
				$category->setSubdomain( ($this->input->post('subdomain')) ? 1 : 0 );
				$category->setMainMenu( ($this->input->post('main_menu')) ? 1 : 0 );
				if( !$category->nonEmpty() ) {
					$category->insert();
					redirect( site_url($this->ctrl_name) );
				} else {
					redirect( site_url($this->ctrl_name) );
				}
	        }
    	}

		$categories = new $this->Categories_model('c', 'places');
		$categories->setParentCategory(0,true);
		$this->template_data->set( 'categories', $categories->populate() );

		$this->load->view($this->ctrl_name . '/add', $this->template_data->get_data());
	}

	public function edit($id) {	

		$category = new $this->Categories_model('c', 'places');
		$category->setCategoryId($id,true);

		if( $this->input->post() ) { 
			$this->form_validation->set_rules('category_name', 'Category Name', 'required');
			if ( $this->form_validation->run() ) {
				$category->setName( $this->input->post('category_name') );
				$category->setSlug( slugify( $this->input->post('category_slug') ) );
				$category->setParentCategory( ($this->input->post('parent_category')) ? $this->input->post('parent_category') : 0 );
				$category->setPriority( $this->input->post('priority')  );
				$category->setActive( ($this->input->post('active')) ? 1 : 0 );
				$category->setSubdomain( ($this->input->post('subdomain')) ? 1 : 0 );
				$category->setMainMenu( ($this->input->post('main_menu')) ? 1 : 0 );
				if( $category->nonEmpty() ) {
					$category->update();
				}
	        }
    	}

		$this->template_data->set( 'category', $category->get() );

		$categories = new $this->Categories_model('c', 'places');
		$categories->setParentCategory(0,true);
		$categories->set_where('c.category_id<>'.$id);
		$this->template_data->set( 'categories', $categories->populate() );

		$this->load->view($this->ctrl_name . '/edit', $this->template_data->get_data());
	}

	public function delete($id) {	
		$category = new $this->Categories_model(NULL, 'places');
		$category->setCategoryId($id,true);
		$category->delete();
		redirect( $this->input->get('next') );
	}

}
