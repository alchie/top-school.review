<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounts_users Controller Class
 *
 * Default Controller of accounts_users database

 * @package			        Accounts_users Controller
 * @version_number	        1.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Generator (CG) v3.5.0
 */
 
class Accounts_users extends MAIN_Controller {

	private $ctrl_title = 'Accounts Users';
	private $ctrl_name = 'accounts_users';

	public function __construct() {
		parent::__construct();

		$this->template_data->set('page_title', $this->ctrl_title);
		$this->template_data->set('page_name', $this->ctrl_name);
	}

	public function index($start=0) {	

		$data = new $this->Accounts_users_model('a', 'tsr_accounts');
		if( $this->input->get('q') ) {
			$data->set_where('a.email LIKE "%'.$this->input->get('q').'%"');
			$data->set_where_or('a.full_name LIKE "%'.$this->input->get('q').'%"');
			
		}
		$data->set_order('a.uid', 'DESC');
		$data->set_start($start);
		$this->template_data->set( 'data', $data->populate() );

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/{$this->ctrl_name}/index/"),
			'total_rows' => $data->count_all_results(),
			'per_page' => $data->get_limit()
		)));
		
		$this->load->view($this->ctrl_name . '/index', $this->template_data->get_data());
	}

	public function add() {


		if( $this->input->post() ) { 
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			
			if ( $this->form_validation->run() ) {
				$data = new $this->Accounts_users_model('a', 'tsr_accounts');
				if( $this->input->post('email') ) {
					$data->setEmail( $this->input->post('email') );
				}
				if( $this->input->post('full_name') ) {
					$data->setFullName( $this->input->post('full_name') );
				}
				if( $this->input->post('password') ) {
					$data->setPassword( $this->input->post('password') );
				}
				if( $this->input->post('type') ) {
					$data->setType( $this->input->post('type') );
				}
				if( $this->input->post('verified') ) {
					$data->setVerified( $this->input->post('verified') );
				}
				if( $this->input->post('code') ) {
					$data->setCode( $this->input->post('code') );
				}
				if( $this->input->post('date_registered') ) {
					$data->setDateRegistered( $this->input->post('date_registered') );
				}
				if( $this->input->post('referred_by') ) {
					$data->setReferredBy( $this->input->post('referred_by') );
				}

				if( $data->insert() ) {
					redirect( site_url($this->ctrl_name) . "?resp_code=201" );
				}

	        }
    	}


		$this->load->view($this->ctrl_name . '/add', $this->template_data->get_data());
	}

	public function edit($id) {	

		$data = new $this->Accounts_users_model('a', 'tsr_accounts');
		$data->setUid($id,true);

                
		if( $this->input->post() ) { 
			$this->form_validation->set_rules('email', 'Email', 'trim|required');
			$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
			
			if ( $this->form_validation->run() ) {
				if( $this->input->post('email') ) {
					$data->setEmail( $this->input->post('email'), false, true );
				}
				if( $this->input->post('full_name') ) {
					$data->setFullName( $this->input->post('full_name'), false, true );
				}
				if( $this->input->post('password') ) {
					$data->setPassword( $this->input->post('password'), false, true );
				}
				if( $this->input->post('type') ) {
					$data->setType( $this->input->post('type'), false, true );
				}
				if( $this->input->post('verified') ) {
					$data->setVerified( $this->input->post('verified'), false, true );
				}
				if( $this->input->post('code') ) {
					$data->setCode( $this->input->post('code'), false, true );
				}
				if( $this->input->post('date_registered') ) {
					$data->setDateRegistered( $this->input->post('date_registered'), false, true );
				}
				if( $this->input->post('referred_by') ) {
					$data->setReferredBy( $this->input->post('referred_by'), false, true );
				}

				if( $data->update() ) {
					redirect( site_url($this->ctrl_name) . "?resp_code=202" );
				}

	        }
    	}


		$this->template_data->set( 'data', $data->get() );

		$this->load->view($this->ctrl_name . '/edit', $this->template_data->get_data());
	}

	public function delete($id) {	
		$data = new $this->Accounts_users_model(NULL, 'tsr_accounts');
		$data->setUid($id,true);
		$data->delete();
		redirect( site_url( $this->input->get('next') ) . "?resp_code=203" );
	}

}
