<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Accounts_facebook Controller Class
 *
 * Default Controller of accounts_facebook database

 * @package			        Accounts_facebook Controller
 * @version_number	        1.0
 * @project			        Trokis Philippines
 * @project_link	        http://www.trokis.com
 * @author			        Chester Alan Tagudin
 * @author_link		        http://www.chesteralan.com
 * @generator		        CodeIgniter Generator (CG) v3.5.0
 */
 
class Accounts_facebook extends MAIN_Controller {

	private $ctrl_title = 'Accounts Facebook';
	private $ctrl_name = 'accounts_facebook';

	public function __construct() {
		parent::__construct();

		$this->template_data->set('page_title', $this->ctrl_title);
		$this->template_data->set('page_name', $this->ctrl_name);
	}

	public function index($start=0) {	

		$data = new $this->Accounts_facebook_model('a', 'tsr_accounts');
		if( $this->input->get('q') ) {
			
		}
		$data->set_order('a.id', 'DESC');
		$data->set_start($start);
		$this->template_data->set( 'data', $data->populate() );

		$this->template_data->set('pagination', bootstrap_pagination(array(
			'base_url' => base_url( $this->config->item('index_page') . "/{$this->ctrl_name}/index/"),
			'total_rows' => $data->count_all_results(),
			'per_page' => $data->get_limit()
		)));
		
		$this->load->view($this->ctrl_name . '/index', $this->template_data->get_data());
	}

	public function add() {


		if( $this->input->post() ) { 
			
			if ( $this->form_validation->run() ) {
				$data = new $this->Accounts_facebook_model('a', 'tsr_accounts');
				if( $this->input->post('user_id') ) {
					$data->setUserId( $this->input->post('user_id') );
				}
				if( $this->input->post('facebook_id') ) {
					$data->setFacebookId( $this->input->post('facebook_id') );
				}
				if( $this->input->post('name') ) {
					$data->setName( $this->input->post('name') );
				}
				if( $this->input->post('email') ) {
					$data->setEmail( $this->input->post('email') );
				}

				if( $data->insert() ) {
					redirect( site_url($this->ctrl_name) . "?resp_code=201" );
				}

	        }
    	}


		$this->load->view($this->ctrl_name . '/add', $this->template_data->get_data());
	}

	public function edit($id) {	

		$data = new $this->Accounts_facebook_model('a', 'tsr_accounts');
		$data->setId($id,true);

                
		if( $this->input->post() ) { 
			
			if ( $this->form_validation->run() ) {
				if( $this->input->post('user_id') ) {
					$data->setUserId( $this->input->post('user_id'), false, true );
				}
				if( $this->input->post('facebook_id') ) {
					$data->setFacebookId( $this->input->post('facebook_id'), false, true );
				}
				if( $this->input->post('name') ) {
					$data->setName( $this->input->post('name'), false, true );
				}
				if( $this->input->post('email') ) {
					$data->setEmail( $this->input->post('email'), false, true );
				}

				if( $data->update() ) {
					redirect( site_url($this->ctrl_name) . "?resp_code=202" );
				}

	        }
    	}


		$this->template_data->set( 'data', $data->get() );

		$this->load->view($this->ctrl_name . '/edit', $this->template_data->get_data());
	}

	public function delete($id) {	
		$data = new $this->Accounts_facebook_model(NULL, 'tsr_accounts');
		$data->setId($id,true);
		$data->delete();
		redirect( site_url( $this->input->get('next') ) . "?resp_code=203" );
	}

}
